DELIMITER $$

USE `dbsgi`$$

DROP PROCEDURE IF EXISTS `SP_S_ProductoPorParametro`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_S_ProductoPorParametro`(IN `pcriterio` VARCHAR(20), IN `pbusqueda` VARCHAR(50), IN `plimit` VARCHAR(50)
        )
BEGIN       
        
    IF pcriterio = "oid_prod" THEN                  
            SET @sentencia = CONCAT("
             SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod
               AND p.oid_prod = '",pbusqueda,"' 
             ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;
        ELSEIF pcriterio = "cod_prod" THEN
            SET @sentencia = CONCAT("
             SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod 
               AND p.cod_prod = '",pbusqueda,"' ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;               
        ELSEIF pcriterio = "nom_prod" THEN
            SET @sentencia = CONCAT("
          SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod
               AND p.nom_prod LIKE '",CONCAT("%",pbusqueda,"%"),"'"," 
                ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;           
        ELSEIF pcriterio = "des_prod" THEN
            SET @sentencia = CONCAT("
          SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod
             WHERE p.nom_prod LIKE '",CONCAT("%",pbusqueda,"%"),"'"," ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;       
        ELSEIF pcriterio = "des_cate_prod" THEN
            SET @sentencia = CONCAT("
          SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod
             WHERE c.des_cate_prod LIKE '",CONCAT("%",pbusqueda,"%"),"'"," ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;                           
        ELSE    
            SET @sentencia = CONCAT("
            SELECT 
              p.oid_prod,
              p.cod_prod,
              p.nom_prod,
              p.des_prod,
              p.val_stoc,
              p.val_stoc_mini,
              p.imp_prec_cost,
              p.imp_prec_vent,
              p.imp_util,
              p.ind_esta,
              p.val_imag,
              c.des_cate_prod 
             FROM 
              mae_produ AS p, 
              mae_categ_produ AS c  
             WHERE p.oid_cate_prod = c.oid_cate_prod 
                ORDER BY p.oid_prod DESC ",plimit);
            PREPARE consulta FROM @sentencia;
            EXECUTE consulta;   
    END IF;         
        
        
    END$$

DELIMITER ;