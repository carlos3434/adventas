/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.22-log : Database - dbsgi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbsgi` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbsgi`;

/*Table structure for table `fac_tipo_docum_legal` */

DROP TABLE IF EXISTS `fac_tipo_docum_legal`;

CREATE TABLE `fac_tipo_docum_legal` (
  `cod_tipo_docu_lega` int(6) NOT NULL AUTO_INCREMENT,
  `des_tipo_docu_lega` varchar(80) NOT NULL,
  `apocope` varchar(11) NOT NULL,
  `Descripcion2` varchar(80) NOT NULL,
  PRIMARY KEY (`cod_tipo_docu_lega`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `fac_tipo_docum_legal` */

insert  into `fac_tipo_docum_legal`(`cod_tipo_docu_lega`,`des_tipo_docu_lega`,`apocope`,`Descripcion2`) values (1,'BOLETA','BOL','BOLETA'),(2,'FACTURA','FAC','FACTURA'),(3,'TICKET FACTURA','TICK F','TICKET FACTURA'),(4,'TICKET BOLETA','TICK B','TICKET BOLETA');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
