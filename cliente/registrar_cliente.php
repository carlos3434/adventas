<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">
<link href="../css/Imagenes.css" rel="stylesheet" type="text/css">
<style>
/* Estilo por defecto para validacion */  
input:required:invalid {  border: 1px solid red;  }  input:required:valid {  border: 1px solid green;  }
</style>
</head>
<?php error_reporting (-1);?>
<?php
include_once("../clases/clsTipoDocumentoIdentidad.php");
$objTipoDocuIden=new clsTipoDocumentoIdentidad;
$result=$objTipoDocuIden->consultarTipoDocumentoIdentidad();
?>
<body>

<div class="wrapper">
<form action="guardar_cliente.php" method="post" name="form_cliente">
<div class="block">
    <div class="block_head"> 
    <div class="imagen_head"><img src="../img/Iconfinder/customer.png" width="48" height="45"></div>
    <div class="titulo_head">REGISTRAR CLIENTE</div>
        <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>
					<td>
            		<button type="submit" class="button">
                   <span class="Guardar" title="Guardar">
                        </span>
                        Guardar
          			</button>
                    </td>                                 
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="Cancelar" title="Cancelar">
                        </span>
                        Cancelar
                        </a>
                    </td>               
                    <td>
                        <a href="#" class="toolbar">
                        <span class="Ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->    
    </div><!--Cierra block_head-->
    <div class="block_content">

    <fieldset class="adminform">
    <legend>Detalle del cliente</legend>
    <table class="admintable">
    <tr>
        <td width="100" class="key">Nombre o Razón Social:</td>
        <td colspan="2"><input type="text" style="text-transform:uppercase;" name="nom_clie" size="100" onkeyup="javascript:this.value=this.value.toUpperCase()" title="Se necesita un nombre o razón social de cliente"  required></td>
    </tr>
    
    <tr>
        <td width="100" class="key">Tipo Documento:</td>
        <td colspan="2">
        
          <select name="cod_tipo_docu_iden" required>
                    <option value="">- Seleccione un Tipo Documento -</option>
          <?php
        while($row=mysql_fetch_array($result)){?>

            <option value="<?php echo $row['cod_tipo_docu_iden']?>"><?php echo $row['des_tipo_docu_iden']?></option>
    <?php } ?>
        </select>

        </td>
    </tr>
    
    <tr>
        <td width="100" class="key">Numero Documento Identidad:</td>
        <td colspan="2"><input type="text" name="num_docu_iden" size="20" pattern="[0-9]{8}"></td>
    </tr>

    <tr>
        <td width="100" class="key">Dirección:</td>
        <td colspan="2"><input type="text" name="val_dire" size="60"></td>
    </tr>
    
    <tr>
        <td width="100" class="key">Teléfono Fijo:</td>
        <td colspan="2"><input type="text" name="val_tele_fijo" size="25"></td>
    </tr>

    <tr>
        <td width="100" class="key">Teléfono Movil 1:</td>
        <td colspan="2"><input type="text" name="val_tele_mov1" size="25"></td>
    </tr>

    <tr>
        <td width="100" class="key">Teléfono Movil 2:</td>
        <td colspan="2"><input type="text" name="val_tele_mov2" size="25"></td>
    </tr>

    <tr>
        <td width="100" class="key">Observación:</td>
        <td colspan="2"><textarea name="val_obse" id="textarea" cols="40" rows="2"></textarea></td> 
    </tr>

     <input id="accion" name="accion" value="guardar" type="hidden">

    </table>
         
    
</fieldset>



</div><!--Cierra Block_Content-->
</div><!--Cierra Wrapper-->
</form>
</div><!--Cierra Block-->



</body>
</html>