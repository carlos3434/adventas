<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">
</head>
<?php error_reporting (0);?>
<body>
<div class="wrapper">
<form name="form_cliente" method="post" action="guardar_cliente.php">
<div class="block">

    <div class="block_head"> 
    	<div class="imagen_head"><img src="../img/header/cliente.png" width="48" height="45"></div>
    <div class="titulo_head">EDITAR CLIENTE</div>
    
      <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>     
                    <td>
<button type="submit" class="button">
                   <span class="icon-32-guardar_editar" title="Guardar">
                        </span>
                        Guardar
          			</button>
                    </td>       
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Nuevo">
                        </span>
                        Cerrar
                        </a>
                    </td>               
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->
    </div><!--Cierra block_head-->
    <div class="block_content">

<?php
include_once("../clases/clsCliente.php");


	$oid_clie=$_GET["oid_clie"];
	
	$objCliente=new clsCliente;
	$resultado=$objCliente->consultarClientePorParametro('oid_clie',$oid_clie,'');

	
	while($row=@mysql_fetch_array($resultado)){
		
		$nom_clie 	        = $row["nom_clie"];
		$cod_tipo_docu_iden = $row["cod_tipo_docu_iden"];
		$des_tipo_docu_iden = $row["des_tipo_docu_iden"];
		$num_docu_iden	    = $row["num_docu_iden"];
		$val_dire	        = $row["val_dire"];
		$val_tele_fijo 		= $row["val_tele_fijo"];
		$val_tele_mov1		= $row["val_tele_mov1"];
		$val_tele_mov2		= $row["val_tele_mov2"];
		$val_obse			= $row["val_obse"];
		$val_usua			= $row["val_usua"];

	}

?>

<?php
include_once("../clases/clsTipoDocumentoIdentidad.php");
$objTipoDocuIden=new clsTipoDocumentoIdentidad;
$result=$objTipoDocuIden->consultarTipoDocumentoIdentidad();
?>


<input type="hidden" name="oid_clie" value="<?php echo $oid_clie ?>">
<input id="accion" name="accion" value="modificar" type="hidden">
    <fieldset class="adminform">
    <legend>Detalles del Cliente</legend>
<table class="admintable">
	<tr>
		<td width="100" class="key">ID:</td>
		<td><?php echo $oid_clie ?></td>
	</tr>
	<tr>
		<td class="key">Nombre o Razón Social:</td>
		<td><input type="text" name="nom_clie" value="<?php echo $nom_clie ?>" size="40" title="Se necesita un nombre o razón social"  required></td>
	</tr>

	<tr>
        <td width="100" class="key">Tipo Documento:</td>
        <td colspan="2">
        
          <select name="cod_tipo_docu_iden" required>
          	  <option value="">- Seleccione un Tipo Documento -</option>
          	  <?php
                 $selected = ''; 
			     while ($row=mysql_fetch_array($result)) {   
        		  if ($row['cod_tipo_docu_iden'] == $cod_tipo_docu_iden)  $selected = "selected = 'selected'";
        		 
        		     echo "<option value='".$row['cod_tipo_docu_iden']."' ".$selected." >".$row['des_tipo_docu_iden']."</option>"; 
    		       $selected = '';
    		  ?>
    		  <?php } ?>
        </select>
    	</td>
    </tr>

	<tr>
		<td class="key">Numero Documento Identidd:</td>
		<td><input type="text" name="num_docu_iden" value="<?php echo $num_docu_iden ?>" size="25"></td>
	</tr>
	
	<tr>
		<td class="key">Dirección:</td>
		<td><input type="text" name="val_dire" value="<?php echo $val_dire ?>" size="60"></td>
	</tr>
	<tr>
		<td class="key">Teléfono Fijo:</td>
		<td><input type="text" name="val_tele_fijo" value="<?php echo $val_tele_fijo ?>" size="25"></td>
	</tr>
	<tr>
		<td class="key">Teléfono Movil 1:</td>
		<td><input type="text" name="val_tele_mov1" value="<?php echo $val_tele_mov1 ?>" size="25"></td>
	</tr>
	<tr>
		<td class="key">Teléfono Movil 2:</td>
		<td><input type="text" name="val_tele_mov2" value="<?php echo $val_tele_mov2 ?>" size="25"></td>
	</tr>
	<tr>
		<td class="key">Observación:</td>
		<td><textarea name="val_obse"  id="textarea" cols="40" rows="2"><?php print($val_obse); ?></textarea></td>
	</tr>        
	<tr>
		<td class="key">Nombre de Usuario:</td>
		<td><input type="text" name="val_usua" value="<?php echo $val_usua ?>" size="25"></td>
	</tr>   
	
</table>
</fieldset>





	</div><!--Cierra Block_Content-->
</div><!--Cierra Wrapper-->
</form>
</div><!--Cierra Block-->

</BODY>
</HTML>