<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">
</head>
<body> 
<?php error_reporting (-1);?> 
<?php

include_once("../clases/clsCategoria.php");
$objcategoria=new clsCategoria;

?>

<?php
include_once("../clases/clsProducto.php");
$objproducto=new clsProducto;

$accion=$_POST["accion"];

$cod_prod = $_POST["cod_prod"];
$nom_prod = $_POST["nom_prod"];
$des_prod = $_POST["des_prod"];
$cod_barr = $_POST["cod_barr"];
$val_stoc = $_POST["val_stoc"];
$val_stoc_mini = $_POST["val_stoc_mini"];
$imp_prec_cost = $_POST["imp_prec_cost"];
$imp_prec_vent = $_POST["imp_prec_vent"];
$ind_esta = $_POST["ind_esta"];
$oid_cate_prod = $_POST["oid_cate_prod"];

$imp_util = $imp_prec_vent - $imp_prec_cost;

if ($accion=="guardar") {
	
	if ($objproducto->
		    agregarProducto(
		    	$cod_prod,
		    	$nom_prod,
		    	$des_prod,
		    	$cod_barr,
		    	$val_stoc,
		    	$val_stoc_mini,
		    	$imp_prec_cost,
		    	$imp_prec_vent,
		    	$imp_util,
		    	$ind_esta,
		    	$oid_cate_prod)==true)
	{						
		$mensaje="Registro grabado correctamente";
	}else{
		$mensaje="Error de grabacion";
	}
}

if ($accion=="modificar") {
	
	$oid_prod = $_POST["oid_prod"];

	$objproducto=new clsProducto;
	
	if ($objproducto->modificarProducto(
						 $oid_prod,
						 $cod_prod,
						 $nom_prod,
						 $des_prod,
						 $cod_barr,
						 $val_stoc,
						 $val_stoc_mini,
						 $imp_prec_cost,
						 $imp_prec_vent,
						 $imp_util,
						 $ind_esta,
						 $oid_cate_prod)==true) {
	
		$mensaje="Registro actualizado correctamente";

	}else{
		$mensaje="Error de grabacion";
	}
}

$resultado_categ=$objcategoria->consultarCategoriaPorParametro('oid_cate_prod',$oid_cate_prod,'');
while($row=@mysql_fetch_array($resultado_categ)){
	$des_cate_prod=$row['des_cate_prod'];
}
?>      
     
<div class="wrapper">
<div class="block">
	<div class="block_head"> 
    <div class="imagen_head"><img src="../img/header/producto.png" width="46" height="43"></div>
    <div class="titulo_head">Gestor de Productos</div>    
		<div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>       
                    <td>
                        <a href="registrar_producto.php" class="toolbar">
                        <span class="icon-32-nuevo" title="Nuevo">
                        </span>
                        Nuevo
                        </a>
                    </td>
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Cerrar">
                        </span>
                        Cerrar
                        </a>
                    </td>                                    
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->        
    </div><!--Cierra block_head-->
    
    <div class="block_content">
    <div class="box-info"><?php echo $mensaje ?></div>

    <br><p>
    
    <fieldset class="adminform">
    <legend>Detalles del producto</legend>
    <table class="admintable">
        <tr>
            <td class="key">Código:</td>
            <td><?php echo $cod_prod?></td>
        </tr>
        <tr>
            <td class="key">Nombre:</td>
            <td><?php echo $nom_prod?></td>
        </tr>
        <tr>
            <td class="key">Descripción:</td>
            <td><?php echo $des_prod?></td>
        </tr>
        <tr>
            <td class="key">Stock:</td>
            <td><?php echo $val_stoc?></td>
        </tr>
        <tr>
            <td class="key">Stock Mínimo:</td>
            <td><?php echo $val_stoc_mini?></td>
        </tr>
        <tr>
            <td class="key">Precio Costo:</td>
            <td><?php echo $imp_prec_cost?></td>
        </tr>
        <tr>
            <td class="key">Precio Venta:</td>
            <td><?php echo $imp_prec_vent?></td>
        </tr>
        <tr>
            <td class="key">Utilidad:</td>
            <td><?php echo $imp_util?></td>
        </tr>
        <tr>
            <td class="key">Estado:</td>
            <td><?php echo $ind_esta?></td>
        </tr>
        <tr>
            <td class="key">Categoría:</td>
            <td><?php echo $des_cate_prod?></td>
        </tr>
        <tr>
          <td class="key">Código de Barras:</td>
          <td><?php echo "<img src='../lib/barcode/barcode.php?encode=EAN-13&bdata=".$cod_barr."&height=50&scale=2&bgcolor=%23FFFFEC&color=%23333366&type=jpg' width='150' height='70'>"; ?></td>
        </tr>
       
    </table>
    </fieldset>

    </div><!--Cierra block_content-->
    </div>

</div>    
</div><!--Cierra block-->
</div><!--Cierra Wrapper-->
</body>
</html>