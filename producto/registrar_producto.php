<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/Imagenes.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">

<!-- Bootstrap CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/basic-template.css" rel="stylesheet" />

<!-- BootstrapValidator CSS -->
<link href="../css/bootstrapValidator.min.css" rel="stylesheet"/>
    
<!-- jQuery and Bootstrap JS -->
<script src="../js/jquery.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
        
<!-- BootstrapValidator -->
<script src="../js/bootstrapValidator.min.js" type="text/javascript"></script>

<style>
/* Estilo por defecto para validacion */  
input:required:invalid {  border: 1px solid red;  }  input:required:valid {  border: 1px solid green;  }
</style>
</head>
<body>
<?php error_reporting (-1);?>
<?php
include_once("../clases/clsCategoria.php");
$objcategoria=new clsCategoria;
$result=$objcategoria->consultarCategoria();
?>



<div class="container">
    <div class="panel panel-default">
        
        <div class="panel-heading clearfix">
            <h2 class="panel-title pull-left" style="padding-top: 7.5px;">Registrar Producto</h2>
                <div class="input-group">
<!--                    <input type="text" class="form-control" placeholder="Search">-->
                    <div class="input-group-btn">
                        <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
                        <button class="btn btn-primary"><i class="glyphicon glyphicon-wrench"></i></button>
                    </div>
                </div>
        </div>
        
        <div class="panel-body">
                <form  id="form_producto" name="form_producto" class="form-horizontal" action="guardar_producto.php" method="post" enctype="multipart/form-data">   
              
                <fieldset>

                <input type="hidden" name="accion" id="accion" value="guardar"/>
                    
                <div class="form-group">
                    <label for="cod_prod" class="control-label col-xs-2">Codigo:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="cod_prod" id="cod_prod" placeholder="Introduce el codigo de producto" size="15"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nom_prod" class="control-label col-xs-2">Nombre:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="nom_prod" id="nom_prod" placeholder="Introduce el nombre del producto" size="15"/>
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="des_prod" class="control-label col-xs-2">Descripción:</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" rows="3" name="des_prod" id="des_prod"></textarea>
                    </div>
                </div>

    
                <div class="form-group">
                    <label class="control-label col-xs-2">Código de Barras:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="cod_barr" size="50"/>
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-xs-2">Stock:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="val_stoc" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2">Stock Minimo:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="val_stoc_mini" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2">Precio Costo:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="imp_prec_cost" />    
                    </div>        
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2">Precio Venta:</label>
                    <div class="col-xs-10">
                        <input type="text" class="form-control" name="imp_prec_vent" />            
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2">Estado:</label>
                    <label class="radio-inline">
                        <input type="radio" name="ind_esta" id="ind_esta_1" value="1" checked="CHECKED">ACTIVO
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="ind_esta" id="ind_esta_0" value="0">INACTIVO
                    </label>
                </div>

                <div class="form-group">
                   <label for="oid_cate_prod" class="control-label col-xs-2">Categoría:</label>
                    <div class="col-xs-10">
                       <select class="form-control" name="oid_cate_prod" id="oid_cate_prod">
                          <option value="">- Seleccione una Categoría -</option>
                            <?php
                              while($row=mysql_fetch_array($result)){?>
                          <option value="<?php echo $row['oid_cate_prod']?>"><?php echo $row['des_cate_prod']?></option>
                             <?php } ?>
                       </select>
                    </div>
                </div>

                <div class="form-group">
                     <div class="col-xs-offset-2 col-xs-10">
                         <button type="submit" class="btn btn-success">Guardar</button>
                         <a class="btn btn-danger" href="index.php" role="button">Cancelar</a>             
                     </div>
                 </div>

            </fieldset>        
            </form>               
        </div>
    </div>
</div>
</body>

<script type="text/javascript">

    $(document).ready(function () {

        var validator = $("#form_producto").bootstrapValidator({
            feedbackIcons: {
                valid: "glyphicon glyphicon-ok",
                invalid: "glyphicon glyphicon-remove", 
                validating: "glyphicon glyphicon-refresh"
            }, 
            fields : {
                cod_prod :{
                    message : "Email address is required",
                    validators : {
                        notEmpty : {
                            message : "Por favor ingresar el codigo de producto"
                        }, 
                        stringLength: {                            
                            max: 15,
                            message: "El codigo del producto es de maximo 15 caracteres"
                        }                        
                    }
                }, 
                nom_prod : {
                    validators: {
                        notEmpty : {
                            message : "Por favor ingresar el nombre del producto"
                        },
                        stringLength : {
                            max: 100,
                            message: "El nombre del producto es de maximo 100 caracteres"
                        }                        
                    }
                }, 
                val_stoc: {
                    validators: {
                        numeric: {
                            message: 'El stock debe ser un número válido',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                val_stoc_mini: {
                    validators: {
                        numeric: {
                            message: 'El stock mínimo debe ser un número válido',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                imp_prec_cost: {
                    validators: {
                        numeric: {
                            message: 'El precio de costo debe ser un número válido',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                imp_prec_vent: {
                    validators: {
                        numeric: {
                            message: 'El precio de venta debe ser un número válido',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
                    }
                },
                oid_cate_prod : {
                    validators: {
                        notEmpty : {
                            message: "Por favor seleccionar una Categoria"
                        }                        
                    }
                } 
                
            }
        });

    /*
        validator.on("success.form.bv", function (e) {
            e.preventDefault();
            $("#registration-form").addClass("hidden");
            $("#confirmation").removeClass("hidden");
        });*/


     $("input[type=text]").keyup(function(){
        $(this).val( $(this).val().toUpperCase() );
     });
     s
   });
</script>

</html>