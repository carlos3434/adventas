<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php error_reporting (0);?>

<?php
include_once("../lib/barcode/barcode.php");
include_once("../clases/clsProducto.php");

$oid_prod=$_GET['oid_prod'];

$objproducto = new clsProducto;
$resultado=$objproducto->consultarProductoPorParametro('oid_prod',$oid_prod,'');

while($row=mysql_fetch_array($resultado)){
		$oid_prod = $row["oid_prod"];
		$cod_prod = $row["cod_prod"];
		$nom_prod = $row["nom_prod"];
		$des_prod = $row["des_prod"];
        $cod_barr = $row["cod_barr"];
		$val_stoc = $row["val_stoc"];
		$val_stoc_mini = $row["val_stoc_mini"];
		$imp_prec_cost = $row["imp_prec_cost"];
		$imp_prec_vent = $row["imp_prec_vent"];
		$imp_util = $row["imp_util"];
		$ind_esta = $row["ind_esta"];
		$val_imag = $row["val_imag"];
		$des_cate_prod = $row["des_cate_prod"];

}


?>
<div class="wrapper">
<div class="block">

    <div class="block_head"> 
    	<div class="imagen_head"><img src="../img/header/producto.png" width="46" height="43"></div>
    	<div class="titulo_head">GESTOR DE PRODUCTOS</div>
		
        
 <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>
                    <td>
                    <?php
                        echo "<a class='toolbar' href=editar_producto.php?oid_prod=".$oid_prod."><span class='icon-32-editar' title='Editar'>
                        </span>Editar</a>"; ?>
     
                    </td>                        
                    <td>
                        <a href="registrar_producto.php" class="toolbar">
                        <span class="icon-32-nuevo" title="Nuevo">
                        </span>
                        Nuevo
                        </a>
                    </td>
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Cerrar">
                        </span>
                        Cerrar
                        </a>
                    </td>                                    
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->                
    </div><!--Cierra block_head-->
    
    <div class="block_content">
    <fieldset class="adminform">
    <legend>Detalles de producto</legend>


<table class="admintable">
	<tr>
		<td width="100" class="key">Codigo:</td>
		<td><?php echo $cod_prod?></td>
	</tr>
    <tr>
      <td class="key">Nombre:</td>
      <td><?php echo $nom_prod?></td>
    </tr>
    <tr>
		<td class="key">Descripción:</td>
		<td><?php echo $des_prod?></td>
	</tr>

    <tr>
      <td class="key">Código de barras:</td>
      <td><?php echo "<img src='../lib/barcode/barcode.php?encode=EAN-13&bdata=".$cod_barr."&height=50&scale=2&bgcolor=%23FFFFEC&color=%23333366&type=jpg' width='150' height='70'>"; ?></td>
    </tr>

    <tr>
		<td class="key">Stock:</td>
		<td><?php echo $val_stoc?></td>
	</tr>
    <tr>
		<td class="key">Stock Mínimo:</td>
		<td><?php echo $val_stoc_mini?></td>
	</tr>
    <tr>
	<td class="key">Precio Costo:</td>
		<td><?php echo $imp_prec_cost?></td>
	</tr>
	<td class="key">Precio Venta:</td>
		<td><?php echo $imp_prec_vent?></td>
	</tr>
	<td class="key">Utilidad:</td>
		<td><?php echo $imp_util?></td>
	</tr>
	
    <td class="key">Estado:</td>
        <td><?php if ($ind_esta=='1') { echo 'Activo'; } else { echo 'Inactivo';} ?></td>
    </tr>

    <td class="key">Categoria:</td>
		<td><?php echo $des_cate_prod?></td>
	</tr>

	
	
	<td class="key">Imagen:</td>
		<td><img src="../producto/foto/<?php echo $imagen ?>" width="160px" height="140px" border="1"></td>
	</tr>
</table>
</fieldset>
    </div><!--Cierra block_content-->
</div><!--Cierra block-->
</div><!--Cierra Wrapper-->
</body>
</html>