<?php 
//Referenciamos la clase clsConexion
include_once("clsConexion.php");

//Implementamos la clase TipoDocumentoIdentidad
class clsTipoDocumentoIdentidad{
 //Constructor	
 function clsTipoDocumentoIdentidad(){
 }	
 
 //Funcion para agregar una nueva TipoDocumentoIdentidad en la BD
 function agregarTipoDocumentoIdentidad($descripcion){
   $con = new clsConexion;
   if($con->conectarse()==true){
     $query = "CALL SP_I_TipoDocumentoIdentidad('$descripcion')";
     $result = @mysql_query($query);
     if (!$result)
	   return false;
     else
       return true;
   }
 }

 function CantidadTipoDocumentoIdentidad(){
   $con = new clsConexion;
   if($con->conectarse()==true){
     $query = "CALL SP_S_CANTIDAD_TipoDocumentoIdentidadS()";
   $result = @mysql_query($query);
   if (!$result)
     return false;
   else
     return $result;
   }
}
 //Modificar empleado en la base de datos
 function modificarTipoDocumentoIdentidad($idTipoDocumentoIdentidad,$descripcion){
   $con = new clsConexion;
   if($con->conectarse()==true){
     $query = "CALL SP_U_TipoDocumentoIdentidad('$idTipoDocumentoIdentidad','$descripcion')";
     $result = @mysql_query($query);
     if (!$result)
	   return false;
     else
       return true;
   }
 }
 function consultarTipoDocumentoIdentidad(){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
     $query = "CALL SP_S_TipoDocumentoIdentidad()";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }
function consultarTotalTipoDocumentoIdentidads(){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
     $query = "CALL SP_S_TipoDocumentoIdentidadCantidadTotal()";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }
 function consultarTipoDocumentoIdentidadPorParametro($criterio,$busqueda,$limite){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){

     $query = "CALL SP_S_TipoDocumentoIdentidadPorParametro('$criterio','$busqueda','$limite')";
	 $result = @mysql_query($query);
	 if (!$result)

	   return false .mysql_error();

	 else
	   return $result;
   }
 } 
 function consultarTipoDocumentoIdentidadIdMaximo(){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
     $query = "CALL SP_S_TipoDocumentoIdentidadIdMaximo()";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }
 
 
}
?>
