<?php

class Carrito{
    public $codigo;
    public $productos;

    function  __construct($id) {
        $this->codigo=$id;
        $this->productos=array();
    }

    //Indico el codigo y recupero un producto si es que existe en ele carrito

    public function obtenerProducto($codigo){
        foreach($this->productos as $indice => $producto){
            if($producto->cod_prod==$codigo){
                return $producto;
            }
        }
        return null;
    }

    //Actualiza la cantidad de items par aun mismo producto que ya existe en el carrito

    public function actualizarCantidad($codigo,$cantidad){
        foreach ($this->productos as $indice => $producto){
            if($producto->cod_prod==$codigo){
                $producto->val_cant +=$cantidad;
            }
        }
    }

    //Agrego o actualizo su cantidad de un producto al carrito
    public function agregarProducto($producto){
        //Busco el producto si ya fue insertado
        $yaIncluido = $this->obtenerProducto($producto->cod_prod);
        if($yaIncluido){
            $this->actualizarCantidad($producto->cod_prod, 1);
        }else{
            $this->productos[]=$producto;
        }
    }
 //Calcular cantidad de productos en el carrito
    public function calcularCantidad(){
        $cantidad = 0;
        foreach($this->productos as $indice => $producto){
            $cantidad += $producto->val_cant;
        }
        return $cantidad;
    }
    //Calcular el valor venta de los productos
    public function calcularValorVenta(){
        $valor_venta=$this->calcularTotalPagar()/1.18;
        return number_format($valor_venta, 2, '.', '');
    }

    //Calcular el igv del valor venta
    public function calcularIgv(){
        $igv=$this->calcularValorVenta()*0.18;
        return number_format($igv, 2, '.', '');
    }

    //Calcular el total a pagar
    public function calcularTotalPagar(){	
		$monto = 0;
        foreach($this->productos as $indice => $producto){
            $monto += $producto->precioTotal();
        }
        return number_format($monto, 2, '.', '');
   }


   //Eliminar carrito
   public function eliminarProducto($codigo){
       $pro2 = array();
       foreach ($this->productos as $indice => $producto){
            if($producto->cod_prod!=$codigo){
               $pro2[]=$producto;
            }          
        }
//        $this->productos=array();
//        foreach($pro2 as $pro){
//           $this->productos[]=$pro;
//        }
       $this->productos=$pro2;
   }

   //Actualizar PrecioTotal
   public function actualizarCantidadIngresada($cod_prod,$val_cant){
        foreach ($this->productos as $indice => $producto){
            if($producto->cod_prod==$cod_prod){
                $producto->val_cant =$val_cant;
            }
        }
    }
   
}
?>
