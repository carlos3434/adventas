<?php 
//referenciamos la clase clsConexion
include_once("clsConexion.php");
error_reporting(-1);

//implementamos la clase empleado
class clsProveedor{
 //constructor	
 function clsProveedor(){
 }	

 function consultarTotalProveedores(){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
     $query = "CALL SP_S_ProveedorCantidadTotal()";
	 $result = @mysql_query($query);
	 if (!$result)
	   return false;
	 else
	   return $result;
   }
 }  

 function consultarProveedorPorParametro($criterio,$busqueda,$limite){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
     $query = "CALL SP_S_ProveedorPorParametro('$criterio','$busqueda','$limite')";
	   $result = @mysql_query($query) or die ('Error '.mysql_error());
	 
   if (!$result)
	   return false;
	 else
	   return $result;
   }
 }

 //inserta un nuevo empleado en la base de datos
 function agregarProveedor(
             $nom_prov,
             $cod_tipo_docu_iden,
             $num_docu_iden,
             $val_dire,
             $val_tele_1,
             $val_tele_2,
             $val_tele_3,
             $val_mail,
             $val_cuen_banc_1,
             $val_cuen_banc_2,
             $val_obse,
             $ind_esta,
             $oid_usua_crea) {
   $con = new clsConexion;
   if($con->conectarse()==true){
     $query = "CALL SP_I_Proveedor('$nom_prov','$cod_tipo_docu_iden','$num_docu_iden','$val_dire','$val_tele_1','$val_tele_2','$val_tele_3','$val_mail','$val_cuen_banc_1','$val_cuen_banc_2','$val_obse','$ind_esta','$oid_usua_crea')";
     $result = @mysql_query($query)  or die ('Error '.mysql_error());
     if (!$result)
	   return false;
     else
       return true;
   }
 }

 //Modificar empleado en la base de datos
 function modificarProveedor(
             $oid_prov,
             $nom_prov,
             $cod_tipo_docu_iden,
             $num_docu_iden,
             $val_dire,
             $val_tele_1,
             $val_tele_2,
             $val_tele_3,
             $val_mail,
             $val_cuen_banc_1,
             $val_cuen_banc_2,
             $val_obse,
             $ind_esta,
             $oid_usua_modi) {
   $con = new clsConexion;
   if($con->conectarse()==true){
     $query = "CALL SP_U_Proveedor(
                '$oid_prov',
                '$nom_prov',
                '$cod_tipo_docu_iden',
                '$num_docu_iden',
                '$val_dire',
                '$val_tele_1',
                '$val_tele_2',
                '$val_tele_3',
                '$val_mail',
                '$val_cuen_banc_1',
                '$val_cuen_banc_2',
                '$val_obse',
                '$ind_esta',
                '$oid_usua_modi')";

     $result = @mysql_query($query) or die ('Error '.mysql_error());
    
     if (!$result)
	   return false;
     else
       return true;
   }
 }
 
 function consultarProveedorIdMaximo(){
   //creamos el objeto $con a partir de la clase clsConexion
   $con = new clsConexion;
   //usamos el metodo conectar para realizar la conexion
   if($con->conectarse()==true){
    $query = "CALL SP_S_ProveedorIdMaximo()";
	  $result = @mysql_query($query);
	  if (!$result)
	   return false;
	  else
	   return $result;
   }
 }
 
}
?>
