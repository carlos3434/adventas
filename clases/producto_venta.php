<?php

class Producto{
    //Atributos
	public $oid_prod;
    public $cod_prod;
    public $nom_prod;
	public $val_stoc;
    public $imp_prec_cost;
    public $imp_prec_vent;
    public $val_cant;


    //Constructor
    public function  __construct($ip,$c,$n,$sp,$cp,$p) {
		$this->oid_prod = $ip;
        $this->cod_prod = $c;
        $this->nom_prod = $n;
	    $this->val_stoc = $sp;
		$this->imp_prec_cost = $cp;
        $this->imp_prec_vent = $p;
        $this->val_cant = 1;
    }

    //Metodos
    public function precioTotal(){
        return number_format($this->imp_prec_vent*$this->val_cant, 2, '.', '');
    }
}
?>
