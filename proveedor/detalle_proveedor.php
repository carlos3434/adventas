<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
</head>
<body>


<?php
include_once("../clases/clsProveedor.php");

$oid_prov=$_GET['oid_prov'];

$objproveedor = new clsProveedor;
$resultado=$objproveedor->consultarProveedorPorParametro('oid_prov',$oid_prov,'');

while($row=@mysql_fetch_array($resultado)){
		$oid_prov = $row["oid_prov"];
		$nom_prov = $row["nom_prov"];
		$des_tipo_docu_iden = $row["des_tipo_docu_iden"];
		$num_docu_iden = $row["num_docu_iden"];
		$val_dire = $row["val_dire"];
		$val_tele_1 = $row["val_tele_1"];
		$val_tele_2 = $row["val_tele_2"];
        $val_tele_3 = $row["val_tele_3"];
		$val_mail = $row["val_mail"];
		$val_cuen_banc_1 = $row["val_cuen_banc_1"];
		$val_cuen_banc_2 = $row["val_cuen_banc_2"];
		$val_obse =  $row["val_obse"];
        $ind_esta = $row["ind_esta"];
		
}


?>
<div class="wrapper">
<div class="block">

    <div class="block_head"> 
    	<div class="imagen_head"><img src="../img/header/categoria.png" width="48" height="48"></div>
    	<div class="titulo_head">Gestor de Proveedores</div>
		
        
 <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>
                    <td>
                    <?php
                        echo "<a class='toolbar' href=editar_proveedor.php?oid_prov=".$oid_prov."><span class='icon-32-editar' title='Editar'>
                        </span>Editar</a>"; ?>
     
                    </td>                        
                    <td>
                        <a href="registrar_proveedor.php" class="toolbar">
                        <span class="icon-32-nuevo" title="Nuevo">
                        </span>
                        Nuevo
                        </a>
                    </td>
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Cerrar">
                        </span>
                        Cerrar
                        </a>
                    </td>                                    
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->                
    </div><!--Cierra block_head-->
    
    <div class="block_content">
    <fieldset class="adminform">
    <legend>Detalles del proveedor</legend>


<table class="admintable">
	<tr>
		<td width="100" class="key">ID:</td>
		<td><?php echo $oid_prov?></td>
	</tr>
    <tr>
		<td class="key">Nombre o Razón Social:</td>
		<td><?php echo $nom_prov?></td>
	</tr>
    <tr>
		<td class="key">Tipo Documento Identidad:</td>
		<td><?php echo $des_tipo_docu_iden?></td>
	</tr>
    <tr>
		<td class="key">Numero Documento Identidad:</td>
		<td><?php echo $num_docu_iden?></td>
	</tr>
    <tr>
		<td class="key">Dirección:</td>
		<td><?php echo $val_dire?></td>
	</tr>
    
    <tr>
		<td class="key">Teléfono 1:</td>
		<td><?php echo $val_tele_1?></td>
	</tr>
    
    <tr>
        <td class="key">Teléfono 2:</td>
        <td><?php echo $val_tele_2?></td>
    </tr>

    <tr>
        <td class="key">Teléfono 3:</td>
        <td><?php echo $val_tele_3?></td>
    </tr>

    <tr>
		<td class="key">Email:</td>
		<td><?php echo $val_mail?></td>
	</tr>

    <tr>
		<td class="key">Cuenta Nº 1:</td>
		<td><?php echo $val_cuen_banc_1?></td>
	</tr>

    <tr>
		<td class="key">Cuenta Nº 2:</td>
		<td><?php echo $val_cuen_banc_2?></td>
	</tr>

    <tr>
        <td class="key">Observación:</td>
        <td><?php echo $val_obse?></td>
    </tr>
    

    <tr>
		<td class="key">Estado:</td>
		<td><?php echo $ind_esta?></td>
	</tr>

</table>
</fieldset>
    </div><!--Cierra block_content-->
</div><!--Cierra block-->
</div><!--Cierra Wrapper-->
</body>
</html>