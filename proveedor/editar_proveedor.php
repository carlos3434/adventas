<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">
<style>
/* Estilo por defecto para validacion */  
input:required:invalid {  border: 1px solid red;  }  input:required:valid {  border: 1px solid green;  }
</style>
</head>
<body>
</head>
<body>
<div class="wrapper">
<form name="form_proveedor" method="post" action="guardar_proveedor.php">
<div class="block">

    <div class="block_head"> 
    	<div class="imagen_head"><img src="../img/header/categoria.png" width="48" height="48"></div>
    <div class="titulo_head">EDITAR PROVEEDOR</div>
    
      <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>     
                    <td>
<button type="submit" class="button">
                   <span class="icon-32-guardar_editar" title="Guardar">
                        </span>
                        Guardar
          			</button>
                    </td>       
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Nuevo">
                        </span>
                        Cerrar
                        </a>
                    </td>               
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->
    </div><!--Cierra block_head-->
    <div class="block_content">
<?php
include_once("../clases/clsProveedor.php");


	
	$oid_prov = $_GET["oid_prov"];
	
	$objproveedor=new clsProveedor;
	$resultado=$objproveedor->consultarProveedorPorParametro('oid_prov',$oid_prov,'');

	
	while($row=@mysql_fetch_array($resultado)){
		
		$oid_prov   		 = $row["oid_prov"];
		$nom_prov   		 = $row["nom_prov"];
		$cod_tipo_docu_iden  = $row["cod_tipo_docu_iden"];
		$num_docu_iden       = $row["num_docu_iden"];
		$val_dire  		     = $row["val_dire"];
		$val_tele_1   	     = $row["val_tele_1"];
		$val_tele_2   	     = $row["val_tele_2"];
		$val_tele_3   		 = $row["val_tele_3"];
		$val_mail    	 	 = $row["val_mail"];
		$val_cuen_banc_1     = $row["val_cuen_banc_1"];
		$val_cuen_banc_2     = $row["val_cuen_banc_2"];
		$val_obse     	     = $row["val_obse"];
		$ind_esta       	 = $row["ind_esta"];

	}

?>

<?php
	include_once("../clases/clsTipoDocumentoIdentidad.php");
	$objTipoDocuIden=new clsTipoDocumentoIdentidad;
	$result=$objTipoDocuIden->consultarTipoDocumentoIdentidad();
?>

<input type="hidden" name="oid_prov" value="<?php echo $oid_prov ?>">
<input id="accion" name="accion" value="modificar" type="hidden">
    <fieldset class="adminform">
    <legend>Detalles del proveedor</legend>
    <table class="admintable">
	<tr>
		<td width="100" class="key">ID:</td>
		<td><?php echo $oid_prov ?></td>
	</tr>
    <tr>
		<td class="key">Nombre o Razón Social:</td>
		<td><input type="text" name="nom_prov" value="<?php echo $nom_prov ?>" size="30" title="Se necesita un nombre o razón social del proveedor"  required></td>
	</tr>
    
	<tr>
        <td width="100" class="key">Tipo Documento:</td>
        <td colspan="2">
        
          <select name="cod_tipo_docu_iden" required>
          	  <option value="">- Seleccione un Tipo Documento -</option>
          	  <?php
                 $selected = ''; 
			     while ($row=mysql_fetch_array($result)) {   
        		  if ($row['cod_tipo_docu_iden'] == $cod_tipo_docu_iden)  $selected = "selected = 'selected'";
        		 
        		     echo "<option value='".$row['cod_tipo_docu_iden']."' ".$selected." >".$row['des_tipo_docu_iden']."</option>"; 
    		       $selected = '';
    		  ?>
    		  <?php } ?>
        </select>
    	</td>
    </tr>

	<tr>
		<td class="key">Numero Documento Identidd:</td>
		<td><input type="text" name="num_docu_iden" value="<?php echo $num_docu_iden ?>" size="25"></td>
	</tr>

    <tr>
		<td class="key">Dirección:</td>
		<td><input type="text" name="val_dire" value="<?php echo $val_dire ?>" size="50"></td>
	</tr>
    
    <tr>
		<td class="key">Teléfono 1:</td>
		<td><input type="text" name="val_tele_1" value="<?php echo $val_tele_1 ?>" size="20"></td>
	</tr>
    
    <tr>
		<td class="key">Teléfono 2:</td>
		<td><input type="text" name="val_tele_2" value="<?php echo $val_tele_2 ?>" size="20"></td>
	</tr>

	<tr>
		<td class="key">Teléfono 3:</td>
		<td><input type="text" name="val_tele_3" value="<?php echo $val_tele_3 ?>" size="20"></td>
	</tr>
    
    <tr>
		<td class="key">Email:</td>
		<td><input type="text" name="val_mail" value="<?php echo $val_mail ?>" size="30"></td>
	</tr>
    <tr>
		<td class="key">Cuenta Nº 1:</td>
		<td><input type="text" name="val_cuen_banc_1" value="<?php echo $val_cuen_banc_1 ?>" size="20"></td>
	</tr>
    <tr>
		<td class="key">Cuenta Nº 2:</td>
		<td><input type="text" name="val_cuen_banc_2" value="<?php echo $val_cuen_banc_2 ?>" size="20"></td>
	</tr>
    
    <tr>
		<td class="key">Observación:</td>
		<td><textarea name="val_obse"  id="textarea" cols="40" rows="2"><?php print($val_obse); ?></textarea></td>
	</tr>

    <tr>
		<td class="key">Estado:</td>
		<td><label><input type="radio" name="ind_esta" value="1" <?php if($ind_esta=='1') print "checked=true"?> />ACTIVO</label>
		<label><input type="radio" name="ind_esta" value="0" <?php if($ind_esta=='0') print "checked=true"?> />INACTIVO</label></td>
	</tr>
    
</table>
</fieldset>




	</div><!--Cierra Block_Content-->
</div><!--Cierra Wrapper-->
</form>
</div><!--Cierra Block-->

</body>
</body>