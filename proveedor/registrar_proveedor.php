<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/Imagenes.css" rel="stylesheet" type="text/css">
<style>
/* Estilo por defecto para validacion */  
input:required:invalid {  border: 1px solid red;  }  input:required:valid {  border: 1px solid green;  }
</style>
</head>
<?php error_reporting (-1);?>
<?php
include_once("../clases/clsTipoDocumentoIdentidad.php");
$objTipoDocuIden=new clsTipoDocumentoIdentidad;
$result=$objTipoDocuIden->consultarTipoDocumentoIdentidad();
?>
<body>

<div class="wrapper">
<form action="guardar_proveedor.php" method="post" name="form_proveedor">
<div class="block">
    <div class="block_head"> 
    <div class="imagen_head"><img src="../img/Iconfinder/provider.png" width="48" height="48"></div>
    <div class="titulo_head">REGISTRAR PROVEEDOR</div>
        <div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>
					<td>
            		<button type="submit" class="button">
                   <span class="Guardar" title="Guardar">
                        </span>
                        Guardar
          			</button>
                    </td>                                 
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="Cancelar" title="Cancelar">
                        </span>
                        Cancelar
                        </a>
                    </td>               
                    <td>
                        <a href="#" class="toolbar">
                        <span class="Ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->    
    </div><!--Cierra block_head-->
    <div class="block_content">



    <fieldset class="adminform">
    <legend>Detalle de proveedor</legend>
    <table class="admintable">
    <tr>
        <td class="key">Nombre o Razón Social:</td>
        <td colspan="2"><input type="text" name="nom_prov" size="30" title="Se necesita un nombre o razón social del proveedor"  required></td>
    </tr>
    
    <tr>
        <td width="100" class="key">Tipo Documento:</td>
        <td colspan="2">
        
          <select name="cod_tipo_docu_iden" required>
                    <option value="">- Seleccione un Tipo Documento -</option>
          <?php
        while($row=mysql_fetch_array($result)){?>

            <option value="<?php echo $row['cod_tipo_docu_iden']?>"><?php echo $row['des_tipo_docu_iden']?></option>
    <?php } ?>
        </select>

        </td>
    </tr>
    
    <tr>
        <td width="100" class="key">Numero Documento Identidad:</td>
        <td colspan="2"><input type="text" name="num_docu_iden" size="20" pattern="[0-9]{8}"></td>
    </tr>
    
    <tr>
		<td class="key">Dirección:</td>
		<td><input type="text" name="val_dire" size="50"></td>
	</tr>
    
    <tr>
		<td class="key">Teléfono 1:</td>
		<td><input type="text" name="val_tele_1" size="20"></td>
	</tr>
    
    <tr>
		<td class="key">Teléfono 2:</td>
		<td><input type="text" name="val_tele_2" size="20"></td>
	</tr>

    <tr>
        <td class="key">Teléfono 3:</td>
        <td><input type="text" name="val_tele_3" size="20"></td>
    </tr>

    <tr>
		<td class="key">Email:</td>
		<td><input type="text" name="val_mail" size="30"></td>
	</tr>

    <tr>
		<td class="key">Cuenta Nº 1:</td>
		<td><input type="text" name="val_cuen_banc_1" size="20"></td>
	</tr>

    <tr>
		<td class="key">Cuenta Nº 2:</td>
		<td><input type="text" name="val_cuen_banc_2" size="20"></td>
	</tr>
    
    <tr>
		<td class="key">Estado:</td>
		<td>
           <label>
              <input name="ind_esta" type="radio" id="estado_0" value="1" checked>
                 ACTIVO
           </label>

          <label>
            <input type="radio" name="ind_esta" value="0" id="estado_1">
                 INACTIVO
          </label>
        </td>
	</tr>
    
    <tr>
		<td class="key">Observación:</td>
		<td>
           <textarea name="val_obse" id="textarea" cols="40" rows="2"></textarea>
           <input id="accion" name="accion" value="guardar" type="hidden">
        </td>
	</tr>

    </table>
         
    
</fieldset>

</div><!--Cierra Block_Content-->
</div><!--Cierra Wrapper-->
</form>
</div><!--Cierra Block-->

</body>
</html>