<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../css/general.css" rel="stylesheet" type="text/css">
<link href="../css/icon.css" rel="stylesheet" type="text/css">
<link href="../css/box.css" rel="stylesheet" type="text/css">
</head>
<?php error_reporting (-1);?>
<body>  
<?php
session_start();
include_once("../clases/clsProveedor.php");
$objproveedor=new clsProveedor;

$accion=$_POST["accion"];

$nom_prov           = $_POST["nom_prov"];
$cod_tipo_docu_iden = $_POST["cod_tipo_docu_iden"];
$des_tipo_docu_iden = $_POST["des_tipo_docu_iden"];
$num_docu_iden      = $_POST["num_docu_iden"];
$val_dire           = $_POST["val_dire"];
$val_tele_1         = $_POST["val_tele_1"];
$val_tele_2         = $_POST["val_tele_2"];
$val_tele_3         = $_POST["val_tele_3"];
$val_mail           = $_POST["val_mail"];
$val_cuen_banc_1    = $_POST["val_cuen_banc_1"];
$val_cuen_banc_2    = $_POST["val_cuen_banc_2"];
$val_obse           = $_POST["val_obse"];
$ind_esta           = $_POST["ind_esta"];
$oid_usua_crea      = $_SESSION['id']; 

if ($accion=="guardar") {

	if ($objproveedor->agregarProveedor(
                             $nom_prov,
                             $cod_tipo_docu_iden,
                             $num_docu_iden,
                             $val_dire,
                             $val_tele_1,
                             $val_tele_2,
                             $val_tele_3,
                             $val_mail,
                             $val_cuen_banc_1,
                             $val_cuen_banc_2,
                             $val_obse,
                             $ind_esta,
                             $oid_usua_crea)==true)
	{
		$result=$objproveedor->consultarProveedorIdMaximo();
		$mensaje="Registro grabado correctamente";
	}else{
		$mensaje="Error de grabacion";
	}
	
	while($row=mysql_fetch_array($result))
	{
		$cod=$row['Maximo'];
	}
}

if ($accion=="modificar") 
{
	$oid_prov=$_POST["oid_prov"];

	$nom_prov           = $_POST["nom_prov"];
    $cod_tipo_docu_iden = $_POST["cod_tipo_docu_iden"];
    $num_docu_iden      = $_POST["num_docu_iden"];
    $val_dire           = $_POST["val_dire"];
    $val_tele_1         = $_POST["val_tele_1"];
    $val_tele_2         = $_POST["val_tele_2"];
    $val_tele_3         = $_POST["val_tele_3"];
    $val_mail           = $_POST["val_mail"];
    $val_cuen_banc_1    = $_POST["val_cuen_banc_1"];
    $val_cuen_banc_2    = $_POST["val_cuen_banc_2"];
    $val_obse           = $_POST["val_obse"];
    $ind_esta           = $_POST["ind_esta"];
    $oid_usua_modi      = $_SESSION['id']; 

	$objproveedor=new clsProveedor;
	if ($objproveedor->modificarProveedor(
                             $oid_prov,
                             $nom_prov,
                             $cod_tipo_docu_iden,
                             $num_docu_iden,
                             $val_dire,
                             $val_tele_1,
                             $val_tele_2,
                             $val_tele_3,
                             $val_mail,
                             $val_cuen_banc_1,
                             $val_cuen_banc_2,
                             $val_obse,
                             $ind_esta,
                             $oid_usua_modi)==true)
	{
		$mensaje="Registro actualizado correctamente";
	}else{
		$mensaje="Error de grabacion1";
	}
}
?>      
     
<div class="wrapper">
<div class="block">
	<div class="block_head"> 
    <div class="imagen_head"><img src="../img/header/categoria.png" width="48" height="48"></div>
    <div class="titulo_head">GESTOR DE PROVEEDORES</div>    
		<div class="toolbar" id="toolbar">
            <table class="toolbar">
            	<tbody>
                	<tr>       
                    <td>
                        <a href="registrar_proveedor.php" class="toolbar">
                        <span class="icon-32-nuevo" title="Nuevo">
                        </span>
                        Nuevo
                        </a>
                    </td>
                    <td>
                        <a href="index.php" class="toolbar">
                        <span class="icon-32-cancelar" title="Cerrar">
                        </span>
                        Cerrar
                        </a>
                    </td>                                    
                    <td>
                        <a href="#" class="toolbar">
                        <span class="icon-32-ayuda" title="Ayuda">
                        </span>
                        Ayuda
                        </a>
                    </td>                   
                    </tr>
            	</tbody>
            </table>
        
        </div><!--Cierra toolbar-->        
    </div><!--Cierra block_head-->
    
    <div class="block_content">
    <div class="box-info"><?php echo $mensaje ?></div>

    <fieldset class="adminform">
    <legend>Detalles del proveedor</legend>
    <table class="admintable">
        <tr>
            <td class="key">ID:</td>
            <td><?php echo $oid_prov?></td>
        </tr>
        <tr>
            <td class="key">Nombre o Razón Social:</td>
            <td><?php echo $nom_prov?></td>
        </tr>
        <tr>
            <td class="key">Tipo Documento Identidad:</td>
            <td><?php echo $des_tipo_docu_iden?></td>
        </tr>
        <tr>
            <td class="key">Numero Documento Identidad:</td>
            <td><?php echo $num_docu_iden?></td>
        </tr>
        <tr>
            <td class="key">Dirección:</td>
            <td><?php echo $val_dire?></td>
        </tr>
        <tr>
            <td class="key">Teléfono 1:</td>
            <td><?php echo $val_tele_1?></td>
        </tr>
        <tr>
            <td class="key">Telefono 2:</td>
            <td><?php echo $val_tele_2?></td>
        </tr>
        <tr>
            <td class="key">Telefono 3:</td>
            <td><?php echo $val_tele_3?></td>
        </tr>
        <tr>
            <td class="key">Email:</td>
            <td><?php echo $val_mail?></td>
        </tr>
        <tr>
            <td class="key">Cuenta Nº 1:</td>
            <td><?php echo $val_cuen_banc_1?></td>
        </tr>
        <tr>
            <td class="key">Cuenta Nº 2:</td>
            <td><?php echo $val_cuen_banc_2?></td>
        </tr>
        
        <tr>
            <td class="key">Observación:</td>
            <td><?php echo $val_obse?></td>
        </tr>

        <tr>
            <td class="key">Estado:</td>
            <td><?php echo $ind_esta?></td>
        </tr>
    </table>
    </fieldset>

    </div><!--Cierra block_content-->
</div><!--Cierra block-->
</div><!--Cierra Wrapper-->
</body>
</html>