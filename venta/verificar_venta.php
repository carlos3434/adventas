<?php error_reporting(0);?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="../css/general.css" rel="stylesheet" type="text/css">
    <link href="../css/icon.css" rel="stylesheet" type="text/css">
    <link href="../css/box.css" rel="stylesheet" type="text/css">

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
  <script type="text/javascript" src="js/venta_ajax.js"></script>
  <script type="text/javascript" src="js/venta.js"></script>
</head>
<body>

<?php
include_once("../clases/clsVenta.php");
include_once("../clases/clsDetalleVenta.php");

$objVenta = new clsVenta;
$result_v =$objVenta->consultarVentaUltimoId();

while ($row_venta=mysql_fetch_array($result_v)) {
    $oid_cabe_docu_lega=$row_venta['oid_cabe_docu_lega'];
    echo '<script language="javascript">alert('.$oid_cabe_docu_lega.');</script>';
}

$result_vd=$objVenta->consultarVentaPorParametro('oid_cabe_docu_lega', $oid_cabe_docu_lega);
while ($row_ventad=mysql_fetch_array($result_vd)) {
    $tipo_doc=$row_ventad['TipoDocumento'];
    $cliente=$row_ventad['Cliente'];
    $empleado=$row_ventad['Empleado'];
    $serie=$row_ventad['val_nume_seri_lega'];
    $numero=$row_ventad['val_nume_docu_lega'];
    $fecha_ven=$row_ventad['fec_fact'];
    $total_ven=$row_ventad['imp_subt_docu'];
    $igv_ven=$row_ventad['imp_impu_docu'];
    $totalpago_ven=$row_ventad['imp_tota_docu'];
}

$estado=$_GET["estado"];
$total_pago=$_GET["total_pago"];
$importe=$_GET["pago"];
?>

<div class="wrapper">
    <div class="block">
        <div class="block_head">
            <div class="imagen_head">
                <img src="../img/header/venta_h.png" width="48" height="48">
            </div>
            <div class="titulo_head">GESTOR DE VENTAS</div>
            <div class="toolbar" id="toolbar">
                <table class="toolbar">
                    <tbody>
                        <tr>
                            <?php
                            if ($estado=="efectuado") { ?>
                            <td>
                                <a href="#" id='imprimir' class="toolbar">
                                    <span class="icon-32-printer" title="Imprimir">
                                    </span>
                                    Imprimir
                                </a>
                            </td>
                            <td>
                                <a href="#" class="toolbar" id='nuevo' onClick="NuevaCompra();">
                                    <span class="icon-32-nuevo" title="Nueva venta"></span>Nueva venta
                                </a>
                            </td>
                            <?php } ?>
                            <td id="toolbar-new">
                                <?php
                                if ($estado=="pendiente") {
                                ?>
                                <a href="index.php" class="toolbar" id='retornar'>
                                    <span class="icon-32-regresar" title="Regresar"></span>Regresar
                                </a>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="#" class="toolbar">
                                    <span class="icon-32-ayuda" title="Ayuda"></span>Ayuda
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!--Cierra toolbar-->
        </div><!--Cierra block_head-->
        <div class="block_content">
            <?php
            if ($estado=="efectuado") {
                echo "<div class='box-info'>Venta realizada con éxito</div><br>";
            ?>
            <div class="zona_total">
                <div class="box_contado">
                    <br>
                    <div class="box_contado_texto">Total a Pagar</div>
                    <div class="box_contado_dato" style="color:#090" > S/. <?php echo number_format($total_pago, 2, '.', ''); ?></div>
                    <br>
                    <div class="box_contado_texto">Importe recibido</div>
                    <div class="box_contado_dato">S/. <?php echo number_format($importe, 2, '.', ''); ?></div>
                    <br>
                    <div class="box_contado_texto">Cambio</div>
                    <div class="box_contado_dato" style="color: #BF6000">S/. <?php echo number_format($importe-$total_pago, 2, '.', ''); ?></div>
                  </div>
            </div><!--Cierra zona total-->

        <?php
        $objDetalleVenta = new clsDetalleVenta;
        $result_det=$objDetalleVenta->consultarDetalleVentaPorParametro('oid_cabe_docu_lega', $oid_cabe_docu_lega);
        ?>
            <div class="zona_impresion">
        <!-- codigo imprimir -->
                <br>
                <table border="0" align="center" width="300px">
                    <tr>
                        <td align="center">
                        .::<strong>Sistema de Ventas</strong>::.<br>
                        Jose Galvez 1368 Nº 1368<br>
                        Cel. 979026684 -  Tel. 455630  R.U.C.: 20477157774
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><?php echo "Fecha/Hora: ".date("Y-m-d H:i:s"); ?></td>
                    </tr>
                    <tr>
                      <td align="center"></td>
                    </tr>
                    <tr>
                        <td>Cliente: <?php echo $cliente; ?></td>
                    </tr>
                    <tr>
                        <td>Cajero: <?php echo $empleado; ?></td>
                    </tr>
                    <tr>
                        <td>Nº de venta: <?php echo $serie." - ".$numero ; ?></td>
                    </tr>
                </table>
                <br>
                <table border="0" align="center" width="300px">
                    <tr>
                        <td>CANT.</td>
                        <td>DESCRIPCIÓN</td>
                        <td align="right">IMPORTE</td>
                    </tr>
                    <tr>
                      <td colspan="3">==========================================</td>
                      </tr>
                    <?php
                    error_reporting(1);
                    while ($row_detalle=mysql_fetch_array($result_det)) {
                        echo "<tr>";
                        echo "<td>".$row_detalle['val_cant']."</td>";
                        echo "<td>".$row_detalle['nom_prod']."</td>";
                        echo "<td align='right'>S/. ".$row_detalle['imp_tota']."</td>";
                        echo "</tr>";
                        $cantidad+=$row_detalle['val_cant'];
                    }
                    ?>
                    <tr>
                    <td>&nbsp;</td>
                    <td align="right"><b>TOTAL:</b></td>
                    <td align="right"><b>S/. <?php echo $totalpago_ven  ?></b></td>
                    </tr>
                    <tr>
                      <td colspan="3">Nº de artículos: <?php echo $cantidad ?></td>
                      </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center">¡Gracias por su compra!</td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center">www.incanatoit.com</td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center">Chiclayo - Perú</td>
                    </tr>
                </table>
                <br>
            </div>
            <?php
            } else if ($estado=="pendiente") {
                echo "<div class='box-warning'>El importe percibido es menor al total a pagar</div><br>";
            }
            ?>
        </div><!--Cierra Block_Content-->
    </div><!--Cierra Wrapper-->
</div><!--Cierra Block-->
</body>
</html>