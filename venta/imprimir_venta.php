<?php

error_reporting(0);

include_once("../clases/clsVenta.php");
include_once("../clases/clsDetalleVenta.php");

$objVenta = new clsVenta;
$result_v=$objVenta->consultarVentaUltimoId();

while ($row_venta=mysql_fetch_array($result_v)) {
    $oid_cabe_docu_lega=$row_venta['oid_cabe_docu_lega'];
}

$result_vd=$objVenta->consultarVentaPorParametro('oid_cabe_docu_lega', $oid_cabe_docu_lega);
while ($row_ventad=mysql_fetch_array($result_vd)) {
    $tipo_doc=$row_ventad['TipoDocumento'];
    $cliente=$row_ventad['Cliente'];
    $empleado=$row_ventad['Empleado'];
    $serie=$row_ventad['val_nume_seri_lega'];
    $numero=$row_ventad['val_nume_docu_lega'];
    $fecha_ven=$row_ventad['fec_fact'];
    $total_ven=$row_ventad['imp_subt_docu'];
    $igv_ven=$row_ventad['imp_impu_docu'];
    $totalpago_ven=$row_ventad['imp_tota_docu'];
}

date_default_timezone_set('America/Lima');

$objDetalle = new clsDetalleVenta;
$result_det=$objDetalle->consultarDetalleVentaPorParametro('oid_cabe_docu_lega', $oid_cabe_docu_lega);

$printer="GP-7635 Series";
//$printer="Microsoft Print to PDF";
$handle = printer_open($printer);
if( $handle ) {
    printer_set_option($handle, PRINTER_MODE, "RAW");
    printer_set_option($handle, PRINTER_TEXT_ALIGN, PRINTER_TA_RIGHT);

    printer_write($handle, "        .::Sistema de Ventas::.         ");
    printer_write($handle, "\n");
    printer_write($handle, "           Jose Galvez N 1368           ");
    printer_write($handle, "\n");
    printer_write($handle, "     Cel. 979026684 -  Tel. 455630     ");
    printer_write($handle, "\n");
    printer_write($handle, "          R.U.C.: 20477157774          ");
    printer_write($handle, "\n");
    printer_write($handle, "    Fecha/Hora: ".date("Y-m-d H:i:s"));
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "    Cliente: ". $cliente);
    printer_write($handle, "\n");
    printer_write($handle, "    Cajero: ". $empleado);
    printer_write($handle, "\n");
    printer_write($handle, "     N de venta: ". $serie." - ".$numero);
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "CANT.    DESCRIPCION             IMPORTE");
    printer_write($handle, "\n");
    printer_write($handle, "----------------------------------------");
    printer_write($handle, "\n");
    //deben sumar 40
    $charCant=9;
    $charNom=20;
    $charImp=11;

    while ($row=mysql_fetch_array($result_det)) {
        $valCant =$row['val_cant'];
        $nomProd =$row['nom_prod'];
        $impTota ="S/. ".$row['imp_tota'];
        $len = strlen($nomProd);
        //spacios despues de cantidad
        $lenCant = str_repeat(' ', ( $charCant-strlen($valCant) ));
        //spacios antes de importe
        $lenImp = str_repeat(' ', ($charImp-strlen($impTota) ));
        if ($len>$charNom) {
            $residuo = fmod($len, $charNom);
            $recorrer = ceil($len / $charNom);
            $j=0;
            $k=$charNom;
            for ($i=1; $i <=$recorrer ; $i++) {
                $html = substr($nomProd, $j, $k);
                if ($i==1) {
                    printer_write($handle, $valCant.$lenCant.$html.$lenImp.$impTota);
                } else {
                    //printer_write($handle, "\n");
                    //printer_write($handle, "         ".$html."           ");
                }
                $j+=$charNom;
                $k+=$charNom;
            }
        } else {
            $len = $charNom-$len;
            printer_write($handle, $valCant.$lenCant.$nomProd.str_repeat(' ', $len).$lenImp.$impTota);
        }
        $cantidad+=$valCant;
        printer_write($handle, "\n");
    }
    $totalpago_ven="TOTAL: S/. ".$totalpago_ven;
    $len = 40-strlen($totalpago_ven);
    printer_write($handle, str_repeat(' ', $len).$totalpago_ven);
    printer_write($handle, "\n");
    printer_write($handle, "N de articulos: ". $cantidad);
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "        ¡Gracias por su compra!        ");
    printer_write($handle, "\n");
    printer_write($handle, "          www.incanatoit.com           ");
    printer_write($handle, "\n");
    printer_write($handle, "            Chiclayo - Peru            ");

    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");
    printer_write($handle, "\n");

    printer_close($handle);
    $rst=array('rst'=>1,'msj'=>'se mando a imprimir');
    echo json_encode($rst);
} else {
    $rst=array('rst'=>2,'msj'=>'no se pudo conectarse a la impresora');
    echo json_encode($rst);
}
