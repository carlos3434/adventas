<?php

include_once("../clases/clsCarritoVenta.php");
include_once("../clases/clsVenta.php");
include_once("../clases/clsTipoDocumento.php");
include_once("../clases/clsCliente.php");
include_once("../clases/clsProducto.php");
session_start();

$objVenta = new clsVenta;
$num_venta=$objVenta->generarNumVenta();

$objTipoDoc=new clsTipoDocumento;
$result_td=$objTipoDoc->consultarTipoDocumento();


while ($row=mysql_fetch_array($result_td)) { 
    //$row['cod_tipo_docu_lega'] ;
    //$row['des_tipo_docu_lega'];
    $tipoDocumento[]=$row;

}
if (!isset ($_POST['action'])) {
    if (isset ($_POST['oid_clie'])) {
        $oid_clie=$_POST["oid_clie"];
    } else {
        $oid_clie=2;
    }
    $objCliente=new clsCliente;
    $result_clie=$objCliente->consultarClientePorParametro('oid_clie', $oid_clie, '');

    while ($row_clie=@mysql_fetch_array($result_clie)) {
        $idcliente=$row_clie['oid_clie'];
        $nomcliente=$row_clie['nom_clie'];
    }
}
//Llenar cesta

//Creamos al objeto carrito vacio
$carrito = new Carrito(session_id());

//Recupero el objeto carrito de la sesion en caso exista
if (isset($_SESSION['carrito']))
  $carrito = $_SESSION['carrito'];

if (isset ($_POST['action'])) {
    switch ($_POST['action']) {
        case 'agregar':
            $cod_prod = $_POST['cod_prod'];
            if ($cod_prod!="") {
               $ventaDAO = new clsVenta;
               $producto = $ventaDAO->consultarProductoPorCodigo($cod_prod);
               $carrito->agregarProducto($producto);
            }
            break;
        case 'vacear':
            $carrito = new Carrito(session_id());
            break;
        case 'eliminar':
            $carrito->eliminarProducto($_POST['cod_prod']);
            break;
        case 'actualizar':
            //echo '<script language="javascript">alert("Actualizar");</script>';
            foreach ($_POST as $cod_prod => $val_cant) {
                if ($cod_prod !='' && (int)$val_cant > 0) {
                    //Actualizamos la cantidad para un mismo producto
                    $carrito->actualizarCantidadIngresada($cod_prod, $val_cant);
                }
            }
            break;
        case 'guardar_directo':
            if ($carrito->calcularTotalPagar()>0) {
                $id_tipodoc=$_POST['idtipodoc'];
                $oid_clie=$_POST['idcliente'];
                //$id_emp=$_POST['txtIdEmp'];
                $val_seri_lega=$_POST['serie'];
                $oid_empl = 1;
                $ind_esta = 1;

                $ventaDAO = new clsVenta();
                $facnum = $ventaDAO->guardarFactura(
                    $carrito->productos,
                    $id_tipodoc,
                    $oid_clie,
                    $oid_empl,
                    $val_seri_lega,
                    $num_venta,
                    $carrito->calcularValorVenta(),
                    $carrito->calcularIgv(),
                    $carrito->calcularTotalPagar(),
                    $ind_esta
                ); // var_dump($facnum);
                $estado='efectuado';
                //header("Location: verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$carrito->calcularTotalPagar());
            } else {
                $estado='pendiente';
                //header('Location: verificar_venta.php?estado=pendiente');
            }
            $respuesta = array(
                'rst' =>1,
                'total_pago'=>$carrito->calcularTotalPagar(),
                'pago'      =>$carrito->calcularTotalPagar(),
                'estado'    =>$estado
                );
            echo json_encode($respuesta);
            exit();
            break;
        case 'guardar':
            $pago=$_POST["pago"];
            if ($pago>$carrito->calcularTotalPagar()&&$carrito->calcularTotalPagar()>0) {
                $id_tipodoc=$_POST['idtipodoc'];
                $id_clie=$_POST['idcliente'];
                //$id_emp=$_POST['txtIdEmp'];
                $serie_fact=$_POST['serie'];
                $id_emp="1";
                //$estado="EMITIDO";
                $productoDAO = new clsVenta();
                $facnum = $productoDAO->guardarFactura(
                    $carrito->productos,
                    $id_tipodoc,
                    $id_clie,
                    $id_emp,
                    $serie_fact,
                    $num_venta,
                    $carrito->calcularValorVenta(),
                    $carrito->calcularIgv(),
                    $carrito->calcularTotalPagar(),
                    1//$estado
                );
                $estado='efectuado';
                //header("Location: verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$pago);
            } else {
                $estado='pendiente';
                //header('Location: verificar_venta.php?estado=pendiente');
            }
            $respuesta = array(
                'rst' =>1,
                'total_pago'=>$carrito->calcularTotalPagar(),
                'pago'      =>$pago,
                'estado'    =>$estado
                );
            echo json_encode($respuesta);
            exit();
            break;
    }
    //Guardo nuevamente el carrito de la session
    $_SESSION['carrito'] = $carrito;
    //header('Location: index.php');
    //exit(); //Paramos el script
    $respuesta = array(
        'rst' =>1,
        'num_venta' => $num_venta,
        'objTipoDoc'=> $tipoDocumento,
        'idcliente' => $idcliente,
        'nomcliente' =>$nomcliente,
        'carrito' =>$carrito,
        'calcularTotalPagarFormato' =>'S/. '.number_format($carrito->calcularTotalPagar(), 2, '.', ''),
        'calcularTotalPagar' =>$carrito->calcularTotalPagar(),
        'calcularCantidad' =>$carrito->calcularCantidad(),
        'calcularValorVenta' =>$carrito->calcularValorVenta(),
        'calcularIgv' =>$carrito->calcularIgv()/*,
        'precioTotal'=>0$producto->precioTotal()*/
         );
    echo json_encode($respuesta);
} else { //var_dump($tipoDocumento);
    $respuesta = array(
        'rst' =>1,
        'num_venta' => $num_venta,
        'objTipoDoc'=> $tipoDocumento,
        'idcliente' => $idcliente,
        'nomcliente' =>$nomcliente,
        'carrito' =>$carrito,
        'calcularTotalPagarFormato' =>'S/. '.number_format($carrito->calcularTotalPagar(), 2, '.', ''),
        'calcularTotalPagar' =>$carrito->calcularTotalPagar(),
        'calcularCantidad' =>$carrito->calcularCantidad(),
        'calcularValorVenta' =>$carrito->calcularValorVenta(),
        'calcularIgv' =>$carrito->calcularIgv()/*,
        'precioTotal'=>0$producto->precioTotal()*/
         );
    echo json_encode($respuesta);
}