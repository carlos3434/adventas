$(function() {
    Carrito.Cargar();
    
    $('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });

    $(document).keydown(function(e){
        var key = e.charCode || e.keyCode;
        if (key=='27' || key=='112' || key=='113' || key=='114' || key=='115' || 
            key=='116' || key=='117' || key=='118' || key=='119' ) {
            return eventos_teclas(key);
        } else {
            return true;
        }
    });


    //$( "input[name$='txt_cod_prod']" ).focus();

    /*$("#txt_cod_prod, #txtCliente2, #txtDato_cliente, #txtPago, #txtDato_producto").keydown(function(e){
        var key = e.charCode || e.keyCode;
        if (  (key > 111 && key < 120) || key==27 ) {
            return eventos_teclas(key);
        } else
            return true;
    });*/
    $("#txt_cod_prod, #txtCliente2").keydown(function(e){
        var key = e.charCode || e.keyCode;
        //TECLAS MINUSCULAS, MAYUSCULAS, NUMEROS, NUMEROS, ,,,enter,izq, derecha
        return ( (key >= 65 && key <= 90) || (key >= 96 && key <= 105) || (key >= 48 && key <= 57) || key==192 || key==8 || key==46 || key==13|| key==37 || key==39);
    });
    $("#txt_cod_prod").keyup(function(e) {
        var key = e.charCode || e.keyCode;
        if(key == 13) // enter
            $("#boton_buscar").trigger("click");
    });
    $("#txtCliente2").keyup(function(e) {
        var key = e.charCode || e.keyCode;
        //obtener el tipo de documento seleccionado
        //var tipo_doc = $('#idtipodocumento').val();
        //if (tipo_doc!=='') {
            if(key == 13) {// enter
                var c = "nom_clie";//$('#idtipodocumento').val();
                var b = $("#txtCliente2").val();
                Ajax.Clientes(c,b,mostrarCli);
            }
        //}
    });
    $('#imprimir').click(function(event) {
        var data ={criterio:'c',busqueda:'b'};
        Ajax.Imprimir(data);
    });
    $('#boton_buscar').click(function(event) {
        //debe buscar por codigo de producto
        limpiarProducto();
        if ($('#txt_cod_prod').val()!=='') {
            Ajax.Productos('cod_prod',$('#txt_cod_prod').val(),mostrarProductos);
        }
    });
});
limpiarProducto=function(){
    $('#txtId2').val('');
    $('#textfield').val('');
    //$('#num_venta').val('');
    $('#textfield2').val('');
    $('#textfield3').val('');
    $('#cod_prod').val('');
};
agregarProducto=function(cod_prod){
    if (cod_prod!=='' && cod_prod!==undefined && cod_prod!=='undefined') {
        
    } else {
        cod_prod = $('#cod_prod').val();
    }
    if (cod_prod!==null && cod_prod!=='null' && cod_prod!=='') {
        Carrito.Agregar(cod_prod);
    }
};
eliminarProducto=function(cod_prod){
    Carrito.Eliminar(cod_prod);
};
limpiarCesta=function(){
    Carrito.Limpiar();
};
NuevaCompra=function(){
    //Carrito.Nuevo();
    window.location.href="index.php";
};
actualizarCesta=function(){
    Carrito.Actualizar();

};
mostrarCli=function(rst){
    //console.log()
    //buscar en el recuadro;
};
CargarDetalleProductos=function(datos){
    var i=0, c=1, html='', precioTotal=0;
    $.each(datos, function(index, data) {
        i++;
        if (c==1) {
            html+="<tr class='row1'>";
            c=2;
        } else {
            html+="<tr class='row0'>";
            c=1;
        }
        precioTotal= data.val_cant * data.imp_prec_vent;
        html+="<td width='10'>"+i+"</td>";
        html+="<td width='30' align='center'>";
            html+='<input type="text" name="'+data.cod_prod+'" value="'+data.val_cant+'" maxlength="3" size="4" style="text-align: center" />';
        html+="</td>";
        html+="<td align='center'>"+data.cod_prod+"</a></td>";
        html+="<td align='left'>"+data.nom_prod+"</td>";
        html+="<td width='25' align='center'>"+data.imp_prec_vent+"</td>";
        html+="<td width='25' align='center'>"+precioTotal+"</td>";
        html+="<td width='25' align='center'>";
            html+="<a href='#' onClick='eliminarProducto(\""+data.cod_prod+"\");'>";
            html+='         <img src="../img/icon/eliminar.png" title="Eliminar producto">';
            html+='        </a>';
        html+="</td>";
        //html+="<td width='1%'>"+data.oid_clie+"</td>";
        html+="</tr>";
    });

    if (html==='') {

        $('#t_detalle').css('display','none');
        $('#mensaje_cesta').css('display','');
    } else {
        $('#t_detalle').css('display','');
        $('#mensaje_cesta').css('display','none');

    }
    $('#tb_detalle').html(html);
};
cargarTipoDocumento=function(datos){
    var html='';

    $.each(datos, function(index, data) {
        html+="<option value='"+data.cod_tipo_docu_lega+"'>"+data.des_tipo_docu_lega+"</option>";
    });
    $('#idtipodocumento').html(html);
};
enviar_formulario_directo=function(){
    //validar si tiene productos
    var precio = $('#total_pagar').html();
    if (precio=='S/. 0.00') {
        alert("El importe total a pagar no es valido");
        return;
    }
    var serie_venta = $('#textfield4').val();
    var idtipodoc_venta = $('#idtipodocumento').val();
    var idclie_venta = $('#txtIdCliente').val();
    Carrito.guardar_directo(idtipodoc_venta, idclie_venta,serie_venta );
    //window.location.href="index.php?action=guardar_directo&serie="+serie_venta+"&idtipodoc="+idtipodoc_venta+"&idcliente="+idclie_venta;
};
//esta funcion viene desde el modal
//es pago al contado
enviar_formulario=function(){
    var pago = document.contado.txtPago.value;
    var serie = document.datos.txtSerie.value;
    var idtipodoc=document.datos.idtipodocumento.value;
    var idcliente=document.datos.txtIdCliente.value;
    Carrito.Guardar(pago, serie, idtipodoc, idcliente);
    //window.location.href="index.php?action=guardar&pago="+pago_venta+"&serie="+serie_venta+"&idtipodoc="+idtipodoc_venta+"&idcliente="+idclie_venta;
};
mostrarClientes_modal=function(datos){
    var i=0, c=1, html='';
    $.each(datos, function(index, data) {
        i++;
        if (c==1) {
            html+="<tr class='row1'>";
            c=2;
        } else {
            html+="<tr class='row0'>";
            c=1;
        }
        cliente = ' onClick="cambiarCliente(\''+data.oid_clie+'\',\''+data.nom_clie+'\');" ';
        html+="<td width='10'>"+i+"</td>";
        html+="<td><a style='cursor: pointer;' "+cliente+" href='#' >"+data.nom_clie+"</a></td>";
        html+="<td align='center'>"+data.num_docu_iden+"</a></td>";
        html+="<td align='center'>"+data.des_tipo_docu_iden+"</td>";
        html+="<td>"+data.val_dire+"</td>";
        html+="<td width='6%' align='center'>"+data.val_tele_fijo+"</td>";
        html+="<td width='1%'>"+data.oid_clie+"</td>";
        html+="</tr>";
    });


    $('#table_body_cliente').html(html);
};
cambiarCliente=function(cod_clie,nom_clie){
    if (cod_clie!=='' && cod_clie!==undefined && cod_clie!=='undefined') {
        $('#txtIdCliente').val(cod_clie);
        $('#txtCliente2').val(nom_clie);
        $('#table_body_cliente').html('');
        $(".modal_close").trigger("click");
        //guardar en sesion
    } else {
    }
    /*
    if (cod_clie!==null && cod_clie!=='null' && cod_clie!=='') {
        Carrito.Agregar(cod_clie);
    }*/
};
mostrarProductos=function(datos){
    //mostrar productos para agregar al carrito
    //$('#num_venta').val(datos.num_venta);
    $('#txtId2').val(datos.imp_prec_cost);
    $('#textfield').val(datos.nom_prod);
    $('#textfield2').val(datos.imp_prec_vent);
    $('#textfield3').val(datos.val_stoc);
    $('#cod_prod').val(datos.cod_prod);
    $("#txt_cod_prod").val('');
    $("#txt_cod_prod").focus();
};
mostrarProductos_modal=function(datos){
    //$('#table_body_producto').html(data);
    var i=0, c=1, html='';
    $.each(datos, function(index, data) {
        i++;
        if (c==1) {
            html+="<tr class='row1'>";
            c=2;
        } else {
            html+="<tr class='row0'>";
            c=1;
        }
        if (data.ind_esta==1) {
            html+="<td align='center'><img src='../img/header/activo.png' title='Activo'></td>";
        } else {
            html+= "<td align='center'><img src='../img/header/inactivo.png' title='Inactivo'></td>";
        }
        producto = ' onClick="agregarProducto(\''+data.cod_prod+'\');" ';
        //html+="<td width='10'>"+i+"</td>";
        html+="<td><a style='cursor: pointer;' "+producto+" href='#'>"+data.cod_prod+"</a></td>";
        html+="<td><a style='cursor: pointer;' "+producto+" href='#'>"+data.nom_prod+"</a></td>";
        html+="<td><a style='cursor: pointer;' "+producto+" href='#'>"+data.des_prod+"</a></td>";
        html+="<td align='center'>"+data.val_stoc+"</a></td>";
        html+="<td align='center'>"+data.imp_prec_vent+"</td>";
        if (data.ind_esta==1) {
            html+="<td align='center'><img src='../img/header/activo.png' title='Activo'></td>";
        } else {
            html+="<td align='center'><img src='../img/header/inactivo.png' title='Inactivo'></td>";
        }
        html+="<td align='center'>"+data.des_cate_prod+"</td>";
        html+="<td width='1%'>"+data.oid_prod+"</td>";
        html+="</tr>";
    });
    $('#table_body_producto').html(html);
};
//modal para buscar producto
enviarParametrosProducto=function(){
    c = $('#cboCriterio_producto').val();
    b = $('#txtDato_producto').val();
    Ajax.Productos(c,b,mostrarProductos_modal);
};

//modal para cliente
enviarParametrosCliente=function(){
    c = $('#cboCriterio_cliente').val();
    b = $('#txtDato_cliente').val();
    Ajax.Clientes(c,b,mostrarClientes_modal);
};

eventos_teclas=function(key){
    if(key=='112'){ // p
        $( "#buscar_producto" ).trigger( "click" );
        //por nombre
        $('#cboCriterio_producto').val('nom_prod');
        $( "#txtDato_producto").focus();
    }
    if(key=='113') { // +
        $('#txt_cod_prod').val('');
        $( "#boton_agregar" ).trigger( "click" );
    }
    if(key=='114') {// c
        $( "#buscar_cliente" ).trigger( "click" );
        //seleccionar por documento de indentidad
        $('#cboCriterio_cliente').val('dni');
        $( "#txtDato_cliente").focus();
    }
    if(key=='115') //g
        enviar_formulario_directo();
    if(key=='116') {//i
        $( "#imprimir" ).trigger( "click" );
        $( "#imp_ticket" ).trigger( "click" );
    }
    if(key=='117') {//i
        $( "#go" ).trigger( "click" );
        $("#txtPago").focus();
    }
    if(key=='118')
        $( "#nuevo" ).trigger( "click" );
    if(key=='119') // l
        $("#limpiar").trigger("click");
    if(key=='27') {// l
        $(".modal_close").trigger("click");
    }
    return false;
};