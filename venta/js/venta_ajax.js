var Ajax={
    Productos:function(c,b,evento){
        var data = {criterio:c,busqueda:b};
        $.ajax({
            url         : 'buscar_producto.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                if (obj.rst==1){
                    if (obj.data.length == 1) {
                        evento(obj.data[0]);
                    } else {
                        evento(obj.data);
                    }
                } else {
                    $('#table_body_producto').html('');
                }
            },
            error: function(){
                console.log();
            }
        });
    },
    Clientes:function(c,b,evento){
        var data = {criterio:c,busqueda:b};
        $.ajax({
            url         : 'buscar_cliente.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                if (obj.rst==1){
                    evento(obj.data);
                } else {
                    $('#table_body_cliente').html('');
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Imprimir:function(data){
        $.ajax({
            url         : 'imprimir_venta.php',
            type        : 'POST',
            cache       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            //data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    alert(obj.msj);
                } else if(obj.rst==2) {
                    alert(obj.msj);
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    TipoDocumento:function(){
        $.ajax({
            url         : 'imprimir_venta.php',
            type        : 'POST',
            cache       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            //data        : data,
            beforeSend : function() {
                
            },
            success : function(rst) {
                //evento(rst);
                if (rst.rst==1) {
                    alert(rst.msj);
                } else if(rst.rst==2) {
                    alert(rst.msj);
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
};

var Carrito={
    Cargar:function(){
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    cargarTipoDocumento(obj.objTipoDoc);
                    $('#num_venta').val(obj.num_venta);
                    $('#total_venta').html(obj.calcularTotalPagarFormato);
                    $('#total_cobrar_venta').html(obj.calcularTotalPagarFormato);
                    $('#txtIdCliente').val(obj.idcliente);
                    $('#txtCliente2').val(obj.nomcliente);

                    $('#calcularCantidad').html(obj.calcularCantidad);
                    $('#calcularValorVenta').html(obj.calcularValorVenta);
                    $('#calcularIgv').html(obj.calcularIgv);
                    $('#total_pagar').html(obj.calcularTotalPagar);
                    $("#txt_cod_prod").val('');
                    //$( "input[name$='txt_cod_prod']" ).focus();
                    CargarDetalleProductos(obj.carrito.productos);
                    $("#txt_cod_prod").focus();
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Agregar:function(cod_prod){
        var data = {action:'agregar',cod_prod:cod_prod};
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    $('#table_body_producto').html('');
                    $(".modal_close").trigger("click");
                    limpiarProducto();
                    //cargarTipoDocumento(obj.objTipoDoc);
                    $('#num_venta').val(obj.num_venta);
                    $('#total_venta').html(obj.calcularTotalPagarFormato);
                    $('#total_cobrar_venta').html(obj.calcularTotalPagarFormato);
                    //$('#txtIdCliente').val(obj.idcliente);
                   // $('#txtCliente2').val(obj.nomcliente);

                    $('#calcularCantidad').html(obj.calcularCantidad);
                    $('#calcularValorVenta').html(obj.calcularValorVenta);
                    $('#calcularIgv').html(obj.calcularIgv);
                    $('#total_pagar').html(obj.calcularTotalPagar);
                    $("#txt_cod_prod").val('');
                    CargarDetalleProductos(obj.carrito.productos);
                    $("#txt_cod_prod").focus();
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Eliminar:function(cod_prod){
        var data = {action:'eliminar',cod_prod:cod_prod};
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    $('#num_venta').val(obj.num_venta);
                    $('#total_venta').html(obj.calcularTotalPagarFormato);
                    $('#total_cobrar_venta').html(obj.calcularTotalPagarFormato);
                    //$('#txtIdCliente').val(obj.idcliente);
                    //$('#txtCliente2').val(obj.nomcliente);

                    $('#calcularCantidad').html(obj.calcularCantidad);
                    $('#calcularValorVenta').html(obj.calcularValorVenta);
                    $('#calcularIgv').html(obj.calcularIgv);
                    $('#total_pagar').html(obj.calcularTotalPagar);
                    $("#txt_cod_prod").val('');
                    CargarDetalleProductos(obj.carrito.productos);
                    $("#txt_cod_prod").focus();
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Limpiar:function(){
        var data = {action:'vacear'};
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    $('#num_venta').val(obj.num_venta);
                    $('#total_venta').html(obj.calcularTotalPagarFormato);
                    $('#total_cobrar_venta').html(obj.calcularTotalPagarFormato);
                    //$('#txtIdCliente').val(obj.idcliente);
                    //$('#txtCliente2').val(obj.nomcliente);

                    $('#calcularCantidad').html(obj.calcularCantidad);
                    $('#calcularValorVenta').html(obj.calcularValorVenta);
                    $('#calcularIgv').html(obj.calcularIgv);
                    $('#total_pagar').html(obj.calcularTotalPagar);
                    $("#txt_cod_prod").val('');
                    CargarDetalleProductos(obj.carrito.productos);
                    $("#txt_cod_prod").focus();
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Nuevo:function(){
        var data = {action:'vacear'};
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    //window.location.href="index.php.php";
                    
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Actualizar:function(){
        //$("#detalle").append("action","actualizar");
        $("#detalle").append("<input type='hidden' value='actualizar' name='action'>");
        var data=$("#detalle").serialize();//.split("txt_").join("").split("slct_").join("");
        //var data = {action:'actualizar'};
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    $('#num_venta').val(obj.num_venta);
                    $('#total_venta').html(obj.calcularTotalPagarFormato);
                    $('#total_cobrar_venta').html(obj.calcularTotalPagarFormato);
                    //$('#txtIdCliente').val(obj.idcliente);
                    //$('#txtCliente2').val(obj.nomcliente);

                    $('#calcularCantidad').html(obj.calcularCantidad);
                    $('#calcularValorVenta').html(obj.calcularValorVenta);
                    $('#calcularIgv').html(obj.calcularIgv);
                    $('#total_pagar').html(obj.calcularTotalPagar);
                    $("#txt_cod_prod").val('');
                    CargarDetalleProductos(obj.carrito.productos);
                    $("#txt_cod_prod").focus();
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    guardar_directo:function(idtipodoc, idcliente, serie){
        /*var idtipodoc=$('#idtipodocumento').val();
        var idcliente =$('#txtIdCliente').val();
        var serie  =$('#textfield4').val();*/
        var data = {
            action:'guardar_directo',
            idtipodoc:idtipodoc,
            idcliente:idcliente,
            serie:serie
        };
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    Carrito.Nuevo();
                    window.location.href="verificar_venta.php?estado="+obj.estado+"&total_pago="+obj.total_pago+"&pago="+obj.pago;
                    //verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$carrito->calcularTotalPagar());
                }
            },
            error: function(){
                //console.log();
            }
        });
    },
    Guardar:function(pago, serie, idtipodoc, idcliente){
        /*var idtipodoc=$('#idtipodocumento').val();
        var idcliente =$('#txtIdCliente').val();
        var serie  =$('#textfield4').val();*/
        var data = {
            action:'guardar',
            pago:pago,
            serie:serie,
            idtipodoc:idtipodoc,
            idcliente:idcliente
        };
        $.ajax({
            url         : 'carrito.php',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                
            },
            success : function(obj) {
                //evento(rst);
                if (obj.rst==1) {
                    //cargarTipoDocumento(obj.objTipoDoc);
                    Carrito.Nuevo();
                    window.location.href="verificar_venta.php?estado="+obj.estado+"&total_pago="+obj.total_pago+"&pago="+obj.pago;
                    //verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$carrito->calcularTotalPagar());
                }
            },
            error: function(){
                //console.log();
            }
        });
    }
};