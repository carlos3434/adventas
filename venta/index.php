<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link href="../css/general.css" rel="stylesheet" type="text/css">
  <link href="../css/Imagenes.css" rel="stylesheet" type="text/css">
  <link href="../css/box.css" rel="stylesheet" type="text/css">
  <link href="../css/venta.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/style_v.css" />
  <style>
    /* Estilo por defecto para validacion */
    input:required:invalid {  border: 1px solid red;  }  input:required:valid {  border: 1px solid green;  }
  </style>

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
  <script type="text/javascript" src="js/venta_ajax.js"></script>
  <script type="text/javascript" src="js/venta.js"></script>

</head>
<body>
  <div class="wrapper">
    <div class="block">
      <div class="block_head">
        <div class="imagen_head">
          <img src="../img/header/venta_h.png" width="48" height="48">
        </div>
        <div class="titulo_head">REGISTRO DE VENTAS</div>
        <div class="toolbar" id="toolbar">
            <table class="toolbar">
              <tbody>
                <tr>
                  <td>
                    <button type="submit" class="button" onClick="enviar_formulario_directo();">
                    <!-- <button type="submit" class="button" onClick="parent:location='index.php?action=guardar_directo';">-->
                      <span class="GenerarVenta" title="Generar Venta">
                      </span>Generar venta
                    </button>
                  </td>
                  <td id="toolbar-new">
                      <a href="#signup" id="go" tabindex="-1" rel="leanModal" name="signup" class="toolbar">
                        <span class="Importe" title="Importe"></span>Importe
                      </a>
                  </td>
                  <td id="toolbar-new">
                      <a href="#" class="toolbar">
                        <span class="Ayuda" title="Ayuda"></span>Ayuda
                      </a>
                  </td>
                </tr>
              </tbody>
            </table>
        </div><!--Cierra toolbar-->
      </div><!--Cierra block_head-->
      <div class="block_content">
        <p>

        <?php
        /*
        include_once("../clases/clsCarritoVenta.php");
        include_once("../clases/clsVenta.php");
        include_once("../clases/clsTipoDocumento.php");
        include_once("../clases/clsCliente.php");
        include_once("../clases/clsProducto.php");
        session_start();

        $objVenta = new clsVenta;
        $num_venta=$objVenta->generarNumVenta();

        $objTipoDoc=new clsTipoDocumento;
        $result_td=$objTipoDoc->consultarTipoDocumento();

        if (isset ($_GET['oid_clie'])) {
            $oid_clie=$_GET["oid_clie"];
        } else {
            $oid_clie=2;
        }
        $objCliente=new clsCliente;
        $result_clie=$objCliente->consultarClientePorParametro('oid_clie', $oid_clie, '');

        while ($row_clie=@mysql_fetch_array($result_clie)) {
            $idcliente=$row_clie['oid_clie'];
            $nomcliente=$row_clie['nom_clie'];
        }

        //Llenar cesta

        //Creamos al objeto carrito vacio
        $carrito = new Carrito(session_id());

        //Recupero el objeto carrito de la sesion en caso exista
        if (isset($_SESSION['carrito']))
          $carrito = $_SESSION['carrito'];

        if (isset ($_GET['action'])) {
            switch ($_GET['action']) {
                case 'agregar':
                    $cod_prod = $_GET['cod_prod'];
                    if ($cod_prod!="") {
                       $ventaDAO = new clsVenta;
                       $producto = $ventaDAO->consultarProductoPorCodigo($cod_prod);
                       $carrito->agregarProducto($producto);
                    }
                    break;
                case 'vacear':
                    $carrito = new Carrito(session_id());
                    break;
                case 'eliminar':
                    $carrito->eliminarProducto((int)$_GET['idProducto']);
                    break;
                case 'actualizar':
                    echo '<script language="javascript">alert("Actualizar");</script>';
                    foreach ($_GET as $cod_prod => $val_cant) {
                        if ($cod_prod !='' && (int)$val_cant > 0) {
                            //Actualizamos la cantidad para un mismo producto
                            $carrito->actualizarCantidadIngresada($cod_prod, $val_cant);
                        }
                    }
                    break;
                case 'guardar_directo':
                    if ($carrito->calcularTotalPagar()>0) {
                        $id_tipodoc=$_GET['idtipodoc'];
                        $oid_clie=$_GET['idcliente'];
                        //$id_emp=$_GET['txtIdEmp'];
                        $val_seri_lega=$_GET['serie'];
                        $oid_empl = 1;
                        $ind_esta = 1;

                        $ventaDAO = new clsVenta();
                        $facnum = $ventaDAO->guardarFactura(
                            $carrito->productos,
                            $id_tipodoc,
                            $oid_clie,
                            $oid_empl,
                            $val_seri_lega,
                            $num_venta,
                            $carrito->calcularValorVenta(),
                            $carrito->calcularIgv(),
                            $carrito->calcularTotalPagar(),
                            $ind_esta
                        ); // var_dump($facnum);
                        header("Location: verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$carrito->calcularTotalPagar());
                    }else{
                        header('Location: verificar_venta.php?estado=pendiente');
                    }
                    exit();
                    break;
                case 'guardar':
                    $pago=$_GET["pago"];
                    if ($pago>$carrito->calcularTotalPagar()&&$carrito->calcularTotalPagar()>0) {
                        $id_tipodoc=$_GET['idtipodoc'];
                        $id_clie=$_GET['idcliente'];
                        //$id_emp=$_GET['txtIdEmp'];
                        $serie_fact=$_GET['serie'];
                        $id_emp="1";
                        $estado="EMITIDO";
                        $productoDAO = new clsVenta();
                        $facnum = $productoDAO->guardarFactura(
                          $carrito->productos,
                          $id_tipodoc,
                          $id_clie,
                          $id_emp,
                          $serie_fact,
                          $num_venta,
                          $carrito->calcularValorVenta(),
                          $carrito->calcularIgv(),
                          $carrito->calcularTotalPagar(),
                          $estado
                        );
                        header("Location: verificar_venta.php?estado=efectuado&total_pago=".$carrito->calcularTotalPagar()."&pago=".$pago);
                    } else {
                        header('Location: verificar_venta.php?estado=pendiente');
                    }
                    exit();
                    break;
            }
            //Guardo nuevamente el carrito de la session
            $_SESSION['carrito'] = $carrito;
            header('Location: index.php');
            exit(); //Paramos el script
        }*/
        ?>

        <?php
        error_reporting(1);
/*
        $cod = $_GET["txt_cod_prod"];
        $objProducto = new clsVenta;

        $producto= $objProducto->consultarProductoPorCodigo($cod);

        $oid_prod = $producto->oid_prod;
        $cod_prod = $producto->cod_prod;
        $nom_prod = $producto->nom_prod;
        $val_stoc = $producto->val_stoc;
        $imp_prec_cost = $producto->imp_prec_cost;
        $imp_prec_vent = $producto->imp_prec_vent;
*/
        /*
        if (isset($_POST['Buscar_codigo'])) {

            $cod = $_POST["txt_cod_prod"];
            $objProducto = new clsVenta;

            $producto= $objProducto->consultarProductoPorCodigo($cod);

            $oid_prod = $producto->oid_prod;
            $cod_prod = $producto->cod_prod;
            $nom_prod = $producto->nom_prod;
            $val_stoc = $producto->val_stoc;
            $imp_prec_cost = $producto->imp_prec_cost;
            $imp_prec_vent = $producto->imp_prec_vent;
        }*/

        ?>

        <div class="zona_producto">
          <form name="form2" method="post" action="index.php">
            <fieldset class="adminform">
              <legend>Datos del Producto</legend>
            <table width="100%" border="0" class="admintable">
              <tr>
                <td class="key">
                  <input type="hidden" name="txtId" id="oid_prod" value="">
                  Código de Barras:  
                </td>
                <td align="20%">
                  <input name="txt_cod_prod" id='txt_cod_prod' type="text" value="" size="17">
                  <input type="button" name="Buscar_codigo"  class="button_p" value="" id="boton_buscar" title="Buscar">
                  &nbsp;&nbsp;&nbsp;
                  <a href="#myprod" rel="leanModal" name="signup" id='buscar_producto' tabindex="-1" class="button_p" title="Buscar producto" style="paddin-top:10px">
                    <img src="../img/Iconfinder/page_edit.png">
                  </a>
                </td>
                <td class="key">
                  <span class="key">
                    <input type="hidden" name="txtId2" id="txtId2" value="<?php /*echo $imp_prec_cost*/ ?>">
                    <input type="hidden" name="cod_prod" id="cod_prod" value="<?php /*echo $imp_prec_cost*/ ?>">
                    Precio:
                  </span>
                </td>
                <td align="20%">
                  <input name="textfield2" type="text" disabled id="textfield2" value="<?php /*echo $imp_prec_vent*/ ?>" size="8" style="text-align:center">
                </td>
              </tr>
              <tr>
                <td class="key">Nombre: </td>
                <td align="20%">
                  <input name="textfield" type="text" disabled id="textfield" value="<?php /*echo $nom_prod*/ ?>" size="35">
                </td>
                <td align="20%" class="key">Stock:</td>
                <td align="20%">
                  <input name="textfield3" type="text" disabled id="textfield3" value="<?php /*echo $val_stoc*/ ?>" size="8" style="text-align:center">
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="20%">&nbsp;</td>
                <td align="20%">&nbsp;</td>
                <td align="center">
                  <a href="#" onClick="agregarProducto();"  class="button_p" id="boton_agregar" >
                    <img src="../img/Iconfinder/agregar_venta.png">
                  </a>
                </td>
              </tr>

            </table>
            </fieldset>
          </form>

          <form name="detalle" id="detalle" action="index.php" method="get">
            <table class="adminlist" id="t_detalle">
              <thead>
                <tr>
                  <th>#</th>
                  <th><a href="#">Cant.</a></th>
                  <th><a href="#">C&oacute;digo</a></th>
                  <th><a href="#">Nombre</a></th>
                  <th><a href="#">Precio unit.</a></th>
                  <th><a href="#">Total</a></th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody class="adminlist" id="tb_detalle">
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="13">&nbsp;</td>
                </tr>
              </tfoot>
            </table>
          <br>
          <center>
            <input type="button" id='limpiar' value="Limpiar Cesta" class="btn_operaciones" style="width:130px;" onClick="limpiarCesta();"/>
            <input type="hidden" name="action" value="actualizar"  style="width:130px;"/>
            <input type="button" id="actualizar" value="Actualizar" class="btn_operaciones"  style="width:130px;" onClick="actualizarCesta();">
          </center>
          <!--<input type="button" value="Grabar" class="btn_operaciones" style="width: 130px" onClick="parent:location='index.php?action=guardar';">-->
             <div class='box-info' id="mensaje_cesta">No tiene ningun producto en la cesta</div>
            
          </form> <!--Cierra form detalle-->
        </div>

        <div class="zona_datos">
          <form name="datos" action="index.php" method="get">
            <table width="100%" border="0">
              <tr>
                <td width="32%">
                  <div class="total_venta" style="font-size:35px"><center id="total_venta"></center>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="box_1">
                    <table width="95%" border="0">
                      <tr>
                        <td align="center"><b>SERIE</b></td>
                        <td align="center"><b>Nº DE VENTA</b></td>
                      </tr>
                      <tr>
                        <td align="center"><input name="txtSerie" type="text" id="textfield4" value="001" size="10" style="text-align:center"></td>
                        <td align="center">
                          <input name="num_venta" type="text" disabled id="num_venta" value="" size="30" style="text-align:center">
                        </td>
                      </tr>
                    </table>
                  </div>
                  <br>
                  <div class="box_1">
                    <table width="95%" border="0">
                      <tr>
                        <td>Nombre del cliente (Opcional): </td>
                      </tr>
                      <tr>
                        <td>
                          <input type="hidden" name="txtIdCliente" id='txtIdCliente' value="">
                          <input name="txtNomClie" type="text" id="txtCliente2" size="35" value="">
                          
                          <a href="#myclie" rel="leanModal" name="signup" id='buscar_cliente' tabindex="-1" class="button_p" title="Buscar cliente">
                            <img src="../img/icon/buscar_detalle.png">
                          </a>
                        </td>
                      </tr>
                      <tr>
                        <td>Tipo de documento: 
                          <select name="idtipodocumento" id='idtipodocumento'>
                            
                          </select>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <br>
                  <div class="box_1">
                    <table width="95%" border="0">
                      <tr>
                        <td class="txt_box">Cantidad de Productos:</td>
                        <td align="right" id="calcularCantidad"><?php /*echo $carrito->calcularCantidad(); */?></td>
                      </tr>
                      <tr>
                        <td class="txt_box">Valor Venta:</td>
                        <td align="right" id="calcularValorVenta"><?php /*echo "S/. ".$carrito->calcularValorVenta();*/ ?></td>
                      </tr>
                      <tr>
                        <td sclass="txt_box">I.G.V. %: </td>
                        <td align="right" id="calcularIgv"><?php /*echo "S/. ".$carrito->calcularIgv();*/ ?></td>
                      </tr>
                    </table>
                    <br>
                    <div class="box_1_1">
                      <table width="100%" border="0">
                        <tr>
                          <td class="txt_box_bottom">Total a Pagar:</td>
                          <td class="txt_box_bottom" id="total_pagar" align="right"><?php /*echo "S/. ". $carrito->calcularTotalPagar();*/ ?></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </form><!-- form datos -->
        </div>

        <?php
        $v_cliente="12345";
        ?>

        <!--                             Modal    myContado                  -->
        <div id="signup">
          <div class="modal-header">
              <div class="txt-title">COBRO AL CONTADO</div>
              <a class="modal_close" href="#" title="Cerrar"></a>
          </div><!-- modal-header -->

          <div class="modal-body">
            <p align="center" class="txt-info"><strong>Total a Cobrar</strong></p>
            <pre style="font-size:30px"><center id="total_cobrar_venta" ><?php /*echo 'S/. '.number_format($carrito->calcularTotalPagar(), 2, '.', ''); */?></center></pre>
            <p align="center" class="txt-info"><strong>Forma de Pago "Contado"</strong></p>
            <div align="center">
              <p align="center" class="txt-info-1">Dinero Recibido</p>
              <form name="contado" action="" onSubmit="enviar_formulario(); return false">
              <input type="hidden" name="tpagar" id="tpagar" value="<?php /*echo $carrito->calcularTotalPagar();*/ ?>">
              <div class="input-prepend input-append">
                <span class="add-on"><b>S/.</b></span>
                <input type="number" name="txtPago" id="txtPago" autocomplete="on" required />
              </div>
              <input type="hidden" name="action" value="guardar" style="width:130px;"/>
              </i>
            </div>
          </div><!-- moda-body -->

          <div class="modal-footer">
            <input type="submit" value="Cobrar dinero recibido" class="btn btn-success"><i class=" icon-shopping-cart">
          </div><!-- moda-footer -->
          </form>
        </div><!-- signup -->
        <!--                             Modal    myProducto                 -->
        <div id="myprod">
          <div class="modal-header">
            <div class="txt-title">BUSCAR PRODUCTO</div>
            <a class="modal_close" href="#" title="Cerrar"></a>
          </div><!-- modal-header -->

          <div class="modal-body">
            <form name="form_producto" action="#" method="post" enctype="multipart/form-data" id="form_producto">
              Filtro:
              <input type="text" name="txtDato_producto" id="txtDato_producto">
              <select name="cboCriterio_producto" id="cboCriterio_producto">
                <option>- Seleccione Criterio -</option>
                <option value="cod_prod">Código</option>
                <option value="nom_prod">Nombre</option>
                <option value="des_prod">Descripción</option>
                <option value="categoria">Categoría</option>
              </select>
              <input type="button" name="buttonP" id="buttonP" onClick="enviarParametrosProducto()" value="Buscar">
            </form>
            <table class="adminlist" cellspacing="1">
              <thead>
                <tr>
                  <th>#</th>
                  <th><a href="#">Código</a></th>
                  <th><a href="#">Nombre</a></th>
                  <th><a href="#">Descripción</a></th>
                  <th><a href="#">Stock</a></th>
                  <th><a href="#">Precio Venta</a></th>
                  <th><a href="#">Estado</a></th>
                  <th><a href="#">Categoría</a></th>
                  <th><a href="#">ID</a></th>
                </tr>
              </thead>
              <tbody class="adminlist" id="table_body_producto">
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="13"></td>
                  </tr>
              </tfoot>
            </table>
          </div><!-- moda-body -->
        </div><!-- Cierra myProducto -->
        <!--                             Modal    myCliente                  -->
        <div id="myclie">
          <div class="modal-header">
            <div class="txt-title">BUSCAR CLIENTE</div>
            <a class="modal_close" href="#" title="Cerrar"></a>
          </div><!-- modal-header -->

          <div class="modal-body">
            <form name="form_cliente" action="#" method="post" enctype="multipart/form-data" id="form_cliente">
              Filtro:
              <input type="text" name="txtDato_cliente" id="txtDato_cliente">
              <select name="cboCriterio_cliente" id="cboCriterio_cliente">
                <option>- Seleccione Criterio -</option>
                <option value="nom_clie">Nombre o Razón Social</option>
                <option value="num_docu_iden">RUC</option>
                <option value="num_docu_iden">DNI</option>
              </select>
              <input type="button" name="buttonC" id="buttonC" onClick="enviarParametrosCliente()" value="Buscar">
            </form>
            <table class="adminlist" cellspacing="1">
              <thead>
                <tr>
                  <th>#</th>
                  <th><a href="#">Nombre</a></th>
                  <th><a href="#">RUC</a></th>
                  <th><a href="#">DNI</a></th>
                  <th><a href="#">Dirección</a></th>
                  <th><a href="#">Teléfono</a></th>
                  <th><a href="#">ID</a></th>
                </tr>
              </thead>
              <tbody class="adminlist" id="table_body_cliente">
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="13"></td>
                </tr>
              </tfoot>
            </table>
          </div><!-- moda-body -->
        </div><!-- Cierra myCliente -->

      </div><!--Cierra Block_Content-->
    </div><!--Cierra Wrapper-->

  </div><!--Cierra Block-->

</body>
</html>